package com.IPASS.API;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.IPASS.Service.ServiceProvider;
import com.IPASS.model.Boot;;

@SuppressWarnings("serial")
public class BotenServlet extends HttpServlet {
	@SuppressWarnings("unchecked")
	@Override
	/*
	 * De do get zorgt er voor dat de delete reqeusten worden afgehandeld door middel van een id
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = resp.getWriter();

		resp.setContentType("application/json");

		resp.setContentType("text/html");
		String pathInfo = req.getPathInfo();
		String type = null;

		JSONObject obj = new JSONObject();
		JSONArray arr = new JSONArray();

		String[] parts = null;

		if (pathInfo.length() != 1) {
			parts = pathInfo.split("/");
			type = parts[1];
		}

		if (type != null) {

			if (type.equals("delete")) {
				int id = Integer.parseInt(parts[2]);
				boolean succes = ServiceProvider.getBootService().Delete(id);

				JSONObject data = new JSONObject();
				data.put("succes", succes);

				arr.add(data);
			}

		} else {
			obj.put("succes", false);
			arr.add(obj);
		}
		out.print(arr.toString());
	}
	/* 
	 * De doPost handeld de wijzig en toevoegen requests af. Alle data die nodig zijn worden door het systeem gevraagd en gecontroleerd
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = resp.getWriter();

		resp.setContentType("application/json");

		resp.setContentType("text/html");
		String pathInfo = req.getPathInfo();
		String type = null;

		JSONObject obj = new JSONObject();
		JSONArray arr = new JSONArray();

		String[] parts = null;

		if (pathInfo.length() != 1) {
			parts = pathInfo.split("/");
			type = parts[1];
		}

		System.out.println("Type: " + type);

		if (type != null) {
			if (type.equals("add")) {

				String naam = req.getParameter("name");
				String borg = req.getParameter("borg");
				String ppd = req.getParameter("prijsperdag");
				String verkoop_prijs = req.getParameter("verkoopprijs");
				String status = req.getParameter("status");

				JSONObject data = new JSONObject();

				if (CheckField(naam, 1, 40) && CheckField(borg, 1, 40) && CheckField(ppd, 1, 40)
						&& CheckField(verkoop_prijs, 1, 40) && CheckField(status, 1, 40)) {

					int statusId = 1;

					if (status.equals("Verkoopbaar"))
						statusId = 2;
					if (status.equals("Afgeschreven"))
						statusId = 3;

					Boot b = new Boot(0, naam, Float.valueOf(ppd), Float.valueOf(borg), statusId,
							Float.valueOf(verkoop_prijs));

					b = ServiceProvider.getBootService().Add(b);

					if (b != null) {
						data.put("succes", true);

						data.put("id", b.getId());
						data.put("naam", b.getNaam());
						data.put("prijsperdag", b.getPrijs_per_dag());
						data.put("borg", b.getBorg());
						data.put("verkoopprijs", b.getVerkoop_prijs());
						data.put("status", b.getStatus());
					}

					arr.add(data);
				} else {
					data.put("succes", false);
					arr.add(data);
				}

			} else if (type.equals("edit")) {
				int id = Integer.parseInt(req.getParameter("id"));
				String naam = req.getParameter("name");
				float prijsperdag = Float.parseFloat(req.getParameter("prijsperdag"));
				float borg = Float.parseFloat(req.getParameter("borg"));
				int status = 1;
				if (req.getParameter("status").equals("Verkoopbaar"))
					status = 2;
				if (req.getParameter("status").equals("Afgeschreven"))
					status = 3;

				float verkoopprijs = Float.parseFloat(req.getParameter("verkoopprijs"));

				if (id != 0 && CheckField(naam, 1, 40) && prijsperdag != 0 && borg != 0 && status != 0
						&& verkoopprijs != 0) {

					Boot k = new Boot(id, naam, prijsperdag, borg, status, verkoopprijs);

					boolean succes = ServiceProvider.getBootService().Update(k);
					JSONObject data = new JSONObject();
					data.put("succes", succes);
					arr.add(data);
				}
			} else {
				obj.put("succes", false);
				arr.add(obj);
			}
		} else {
			obj.put("succes", false);
			arr.add(obj);
		}
		out.print(arr.toString());
	}

	private boolean CheckField(String data, int min, int max) {

		if (data.length() < min && data.length() > max) {
			return false;
		}

		return true;
	}
}
