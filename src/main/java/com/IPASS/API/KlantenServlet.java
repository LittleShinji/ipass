package com.IPASS.API;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.IPASS.Service.ServiceProvider;
import com.IPASS.model.Klant;;

@SuppressWarnings("serial")
public class KlantenServlet extends HttpServlet {
	@SuppressWarnings("unchecked")
	@Override
	/*
	 * De do get zorgt er voor dat de delete reqeusten worden afgehandeld door middel van een id
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = resp.getWriter();

		resp.setContentType("application/json");

		resp.setContentType("text/html");
		String pathInfo = req.getPathInfo();
		String type = null;

		JSONObject obj = new JSONObject();
		JSONArray arr = new JSONArray();

		String[] parts = null;

		if (pathInfo.length() != 1) {
			parts = pathInfo.split("/");
			type = parts[1];
		}

		if (type != null) {
			
			if (type.equals("delete")) {
				int id = Integer.parseInt(parts[2]);
				boolean succes = ServiceProvider.getKlantService().Delete(id);

				JSONObject data = new JSONObject();
				data.put("succes", succes);

				arr.add(data);
			}


		} else {
			obj.put("succes", false);
			arr.add(obj);
		}
		out.print(arr.toString());
	}

	@SuppressWarnings("unchecked")
	@Override
	/* 
	 * De doPost handeld de wijzig en toevoegen requests af. Alle data die nodig zijn worden door het systeem gevraagd en gecontroleerd
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = resp.getWriter();

		resp.setContentType("application/json");

		resp.setContentType("text/html");
		String pathInfo = req.getPathInfo();
		String type = null;

		JSONObject obj = new JSONObject();
		JSONArray arr = new JSONArray();

		String[] parts = null;

		if (pathInfo.length() != 1) {
			parts = pathInfo.split("/");
			type = parts[1];
		}

		if (type != null) {
			if (type.equals("edit")) {

				int id = Integer.parseInt(req.getParameter("id"));
				String naam = req.getParameter("name");
				String email = req.getParameter("email");
				String adres = req.getParameter("adres");
				String woonplaats = req.getParameter("city");
				String postcode = req.getParameter("zipcode");
				String Bankrekeningnummer = req.getParameter("iban");

				JSONObject data = new JSONObject();
				
				if (id != 0 && naam != null && email != null && adres != null && woonplaats != null &&
						postcode != null && Bankrekeningnummer != null) {
					Klant k = new Klant(id, naam, adres, email, woonplaats, postcode, Bankrekeningnummer);

					boolean succes = ServiceProvider.getKlantService().Update(k);
					data.put("succes", succes);
					arr.add(data);
				} else {
					data.put("succes", false);
					arr.add(data);
				}
				
			} else if (type.equals("add")) {
				String naam = req.getParameter("name");
				String email = req.getParameter("email");
				String adres = req.getParameter("adres");
				String woonplaats = req.getParameter("city");
				String postcode = req.getParameter("zipcode");
				String Bankrekeningnummer = req.getParameter("iban");
				
				if (CheckField(naam, 1, 40) && CheckField(email, 1, 40) && CheckField(adres, 1, 40) && CheckField(woonplaats, 1, 40) &&
						CheckField(postcode, 1, 7) && CheckField(Bankrekeningnummer, 1, 40)) {
					Klant k = new Klant(naam, adres, email, woonplaats, postcode, Bankrekeningnummer);
					
					k = ServiceProvider.getKlantService().Add(k);
					JSONObject data = new JSONObject();
					
					if (k != null) {
						data.put("succes", true);
					} else {
						data.put("succes", false);
					}
					arr.add(data);
				}
			} else {
				obj.put("succes", false);
				arr.add(obj);
			}
		} else {
			obj.put("succes", false);
			arr.add(obj);
		}
		out.print(arr.toString());
	}
private boolean CheckField(String data, int min, int max) {
		
		if (data.length() < min && data.length() > max) {
			return false;
		}
		
		return true;
	}
}
