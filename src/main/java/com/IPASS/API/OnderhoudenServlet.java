package com.IPASS.API;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.IPASS.Service.ServiceProvider;
import com.IPASS.model.Boot;
import com.IPASS.model.Onderhoud;;

@SuppressWarnings("serial")
public class OnderhoudenServlet extends HttpServlet {
	@SuppressWarnings("unchecked")
	@Override
	/*
	 * De do get zorgt er voor dat de delete reqeusten worden afgehandeld door middel van een id
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = resp.getWriter();

		resp.setContentType("application/json");

		resp.setContentType("text/html");
		String pathInfo = req.getPathInfo();
		String type = null;

		JSONObject obj = new JSONObject();
		JSONArray arr = new JSONArray();

		String[] parts = null;

		if (pathInfo.length() != 1) {
			parts = pathInfo.split("/");
			type = parts[1];
		}

		if (type != null) {

			if (type.equals("delete")) {
				int id = Integer.parseInt(parts[2]);
				boolean succes = ServiceProvider.getOnderhoudService().Delete(id);

				JSONObject data = new JSONObject();
				data.put("succes", succes);

				arr.add(data);
			}

		} else {
			obj.put("succes", false);
			arr.add(obj);
		}
		out.print(arr.toString());
	}
	
	/* 
	 * De doPost handeld de wijzig en toevoegen requests af. Alle data die nodig zijn worden door het systeem gevraagd en gecontroleerd
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = resp.getWriter();

		resp.setContentType("application/json");

		resp.setContentType("text/html");
		String pathInfo = req.getPathInfo();
		String type = null;

		JSONObject obj = new JSONObject();
		JSONArray arr = new JSONArray();

		String[] parts = null;

		if (pathInfo.length() != 1) {
			parts = pathInfo.split("/");
			type = parts[1];
		}

		if (type != null) {
			if (type.equals("add")) {
				if (CheckField(req.getParameter("start-date"), 1, 13) && CheckField(req.getParameter("end-date"), 1, 13)
						&& CheckField(req.getParameter("id-boot"), 1, 13)) {

					Date startdatum = convertStringToDate(req.getParameter("start-date"));
					Date einddatum = convertStringToDate(req.getParameter("end-date"));
					Boot boot = ServiceProvider.getBootService()
							.getBootById(Integer.parseInt(req.getParameter("id-boot")));
					
					Onderhoud b = new Onderhoud(startdatum, einddatum, boot);

					Onderhoud onderhoud = ServiceProvider.getOnderhoudService().Add(b);

					JSONObject data = new JSONObject();
					
					if (onderhoud != null) {
						data.put("succes", true);
						data.put("id", onderhoud.getId());
						data.put("startdatum", convertDateToString(startdatum));
						data.put("einddatum", convertDateToString(einddatum));

						data.put("boot-id", boot.getId());
						data.put("boot-naam", boot.getNaam());
						data.put("boot-prijsperdag", boot.getPrijs_per_dag());
						data.put("boot-verkoopprijs", boot.getVerkoop_prijs());
						data.put("boot-borg", boot.getBorg());
						data.put("boot-status", boot.getStatus());
					}
					arr.add(data);
				}
			} else {
				obj.put("succes", false);
				arr.add(obj);
			}
		} else {
			obj.put("succes", false);
			arr.add(obj);
		}
		out.print(arr.toString());
	}

	private String convertDateToString(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String format = formatter.format(date);
		System.out.println("To String: " + format);
		return format;
	}

	private Date convertStringToDate(String dateString) {
		Date formatted = null;

		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String dateInString = dateString;

		try {

			Date date = formatter.parse(dateInString);
			formatted = date;

		} catch (ParseException e) {
			e.printStackTrace();
		}

		System.out.println("To Date: " + formatted);
		return formatted;
	}

	private boolean CheckField(String data, int min, int max) {

		if (data.length() < min && data.length() > max) {
			return false;
		}

		return true;
	}
}
