package com.IPASS.Controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.IPASS.Service.ServiceProvider;
import com.IPASS.model.Gebruiker;

@SuppressWarnings("serial")
public class LoginServlet extends HttpServlet {
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = resp.getWriter();

		String username = req.getParameter("username");
		String password = req.getParameter("password");

		resp.setContentType("text/html");

		if (!req.getParameter("username").isEmpty() && !req.getParameter("password").isEmpty()) {
			String MD5Pass = "";
			MessageDigest md;
			try {
				md = MessageDigest.getInstance("MD5");
				md.update(password.getBytes());
				byte[] digest = md.digest();
				StringBuffer sb = new StringBuffer();
				for (byte b : digest) {
					sb.append(String.format("%02x", b & 0xff));
				}
				MD5Pass = sb.toString();

			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(MD5Pass);
			Gebruiker g = new Gebruiker(username, MD5Pass);
			if (ServiceProvider.getGebruikerDAO().checkLogin(MD5Pass)) {

				if (req.getParameter("remember") != null) {
					Cookie c = new Cookie("loggedUsername", username);
					c.setMaxAge(60 * 60 * 24);
					resp.addCookie(c);
					c = new Cookie("loggedPassword", password);
					c.setMaxAge(60 * 60 * 24);
					resp.addCookie(c);
				} else {
					Cookie cookie = new Cookie("loggedUsername", null); // Not necessary, but saves bandwidth.
					cookie.setMaxAge(0); // Don't set to -1 or it will become a session cookie!
					resp.addCookie(cookie);
					cookie = new Cookie("loggedPassword", null); // Not necessary, but saves bandwidth.
					cookie.setMaxAge(0); // Don't set to -1 or it will become a session cookie!
					resp.addCookie(cookie);
				}
				req.getSession().setAttribute("LoggedUser", g);
				resp.sendRedirect("/admin/");

			} else {
				String error = "U heeft een ongeldig geberuikersnaam en/of wachtwoord ingevoerd.";
				req.setAttribute("error", error);
				req.setAttribute("username", req.getParameter("username"));
				RequestDispatcher view = req.getRequestDispatcher("index.jsp");
				view.forward(req, resp);
			}
		} else {
			String error = "";

			if (req.getParameter("username").isEmpty())
				error += "U heeft geen gebruikersnaam ingevult";
			else
				req.setAttribute("username", req.getParameter("username"));

			if (req.getParameter("username").isEmpty() && req.getParameter("password").isEmpty())
				error += "<br />";
			if (req.getParameter("password").isEmpty())
				error += "U heeft geen wachtwoord ingevult";

			out.print(error);
			req.setAttribute("error", error);
			RequestDispatcher view = req.getRequestDispatcher("index.jsp");
			view.forward(req, resp);
		}

	}
}
