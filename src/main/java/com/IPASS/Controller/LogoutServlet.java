package com.IPASS.Controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class LogoutServlet extends HttpServlet {
	@Override
	
	/* verwijderd de sessie */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		
		req.getSession().removeAttribute("loggedUser");
		req.getSession().invalidate();
		
		resp.sendRedirect("/");
	}
}
