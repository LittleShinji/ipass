package com.IPASS.Controller;

import java.io.File;
import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class TemplateServlet extends HttpServlet {
	@Override
	
	/* De servlet haalt de url op daaruit wordt er bepaald of de pagina bestaat of niet. */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//PrintWriter out = resp.getWriter();
			resp.setContentType("text/html");
			String pathInfo = req.getPathInfo();
			String pathContent = "index.jsp";
			if (pathInfo.length() != 1) {
				String[] parts = pathInfo.split("/");
				pathContent = parts[1] + ".jsp";
			}
			
			File file = new File(req.getServletContext().getRealPath("/pages/" + pathContent));
			if(file.exists()){
				req.setAttribute("PathContent", pathContent);
				
				if (pathInfo.length() != 1) {
					String[] parts = pathInfo.split("/");
					for (int i = 2; i < parts.length; i++) {
						req.setAttribute("URLDATA" + i, parts[i]);
					}
				}
				
				RequestDispatcher view = req.getRequestDispatcher("/template/template.jsp");
				view.forward(req, resp);
			} else {
				RequestDispatcher view = req.getRequestDispatcher("/template/404.jsp");
				view.forward(req, resp);
			}
		
	}
}
