package com.IPASS.Service;

import static java.lang.Math.toIntExact;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.IPASS.model.Boeking;
import com.IPASS.model.BoekingDAO;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

public class BoekingService {
	private BoekingDAO boekingDAO;

	public BoekingService() {
		this.boekingDAO = new BoekingDAO();
	}

	public List<Boeking> getAll() {
		return this.boekingDAO.getListBoekingen();
	}

	public Boeking getBootById(int id) {

		return this.boekingDAO.getBoekingen(id);
	}

	@SuppressWarnings("deprecation")
	public int PickUpBookings() {
		int count = 0;
		
		Date today = new Date();
		today = new Date(today.getYear(), today.getMonth(), today.getDate());
		
		for (Boeking item: this.boekingDAO.getListBoekingen()) {
			if (item.getStartdatum().getTime() == today.getTime()) {
				count ++;
			}
		}
		
		return count;
	}

	@SuppressWarnings("deprecation")
	public int TakeBackBookings() {
		int count = 0;

		Date today = new Date();
		today = new Date(today.getYear(), today.getMonth(), today.getDate());
		
		for (Boeking item: this.boekingDAO.getListBoekingen()) {
			if (item.getEinddatum().getTime() == today.getTime()) {
				count ++;
			}
		}
		
		return count;
	}

	public int Count() {
		return this.boekingDAO.Count();
	}

	public float TotalRevenue() {
		float revenue = 0;
		//System.out.println("Omzet totaal");
		for (Boeking item : this.boekingDAO.getListBoekingen()) {
			revenue = revenue + (item.getBoot().getPrijs_per_dag() * diffenceBetweenDates(item.getStartdatum(), item.getEinddatum()));
		}

		return revenue;
	}

	private Date getLastDayOfTheMonth(Date date) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.add(Calendar.DATE, -1);
		//System.out.println("Laatste dag van de maand: " + calendar.getTime());
		return calendar.getTime();

	}

	private Date getFirstDayOfTheMonth(Date date) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		calendar.add(Calendar.MONTH, 0);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		//System.out.println("Eerste dag van de maand: " + calendar.getTime());
		return calendar.getTime();

	}

	public float ExpectedRevenueThisMonth() {
		float revenue = 0;
		//System.out.println("Deze maand verwachte omzet");
		for (Boeking item : this.boekingDAO.getListBoekingen()) {
			Date fd = new Date();
			Date ld = new Date();
			ld = getLastDayOfTheMonth(ld);
			fd = getFirstDayOfTheMonth(fd);
			if (fd.getTime() < item.getEinddatum().getTime() && ld.getTime() > item.getEinddatum().getTime()) {
				revenue = (revenue + item.getBoot().getPrijs_per_dag() * diffenceBetweenDates(item.getStartdatum(), item.getEinddatum()));
			}
		}

		return revenue;
	}

	private Date getNextMonth(Date date) {
		
		Calendar calendar = Calendar.getInstance(); 
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, 1);
		calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		date = calendar.getTime();
		//System.out.println("Volgende maand: " + date);
		return date;
	}
	
	private int diffenceBetweenDates(Date start, Date end) {
		int diffence = 0;
		try {
			long diff = end.getTime() - start.getTime();

			diffence = toIntExact((TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) + 1));
		} catch (ParseException e) {
			//System.out.println(e);
		}
		return diffence;
	}

	public float ExpectedRevenueNextMonth() {
		float revenue = 0;
		//System.out.println("Volgende maand verwachte omzet");
		for (Boeking item : this.boekingDAO.getListBoekingen()) {
			Date fd = new Date();
			Date ld = new Date();
			ld = getLastDayOfTheMonth(getNextMonth(ld));
			fd = getNextMonth(fd);
			//System.out.println("Nextmonth: " + fd + " Nextmonth: " + ld + " Nextmonth: " + item.getEinddatum() + "Cost: " + (item.getBoot().getPrijs_per_dag() * diffenceBetweenDates(item.getStartdatum(), item.getEinddatum())));
			if (fd.getTime() < item.getEinddatum().getTime() && ld.getTime() > item.getEinddatum().getTime()) {
				revenue = revenue + (item.getBoot().getPrijs_per_dag() * (diffenceBetweenDates(item.getStartdatum(), item.getEinddatum())));
			}
		}
		
		return revenue;
	}

	public float ThisMonthRevenue() {
		float revenue = 0;
		//System.out.println("Deze maand omzet");
		for (Boeking item : this.boekingDAO.getListBoekingen()) {
			Date fd = new Date();
			Date cd = new Date();
			fd = getFirstDayOfTheMonth(fd);
			if (fd.getTime() < item.getEinddatum().getTime() && cd.getTime() > item.getEinddatum().getTime()) {
				//System.out.println(revenue);
				revenue = revenue + (item.getBoot().getPrijs_per_dag() * diffenceBetweenDates(item.getStartdatum(), item.getEinddatum()));
			}
		}
		
		return revenue;
	}
	
	public int getNewId() {
		int id = 0;
		for (Boeking item: this.boekingDAO.getListBoekingen()) {
			if (item.getId() >= id) {
				id = item.getId() + 1;
			}
		}
		return id;
	}

	public Boeking Add(Boeking b) {
		return this.boekingDAO.Add(b);
	}

	public boolean Delete(int b) {
		if (this.boekingDAO.getBoekingen(b) != null)
			return this.boekingDAO.Delete(b);
		
		return false;
	}
}
