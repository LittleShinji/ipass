package com.IPASS.Service;


import java.util.List;

import com.IPASS.model.Boot;
import com.IPASS.model.BootDAO;

public class BootService {
	BootDAO bootDAO;

	public BootService() {
		this.bootDAO = new BootDAO();
	}
	
	public List<Boot> getAll() {
		return this.bootDAO.getListBoten();
	}
	
	public Boot getBootById(int id) {
		return this.bootDAO.getBoot(id);
	}
	
	public int Count() {
		return this.bootDAO.Count();
	}
	
	public int Countsalable() {
		
		return this.bootDAO.Countsabale();
	}
	
	
	public int Countleasable() {
		return this.bootDAO.Countleasable();
	}
	
	public Boot Add(Boot b) {
		return this.bootDAO.Add(b);
	}
	
	public boolean Update(Boot b) {
		if (this.bootDAO.getBoot(b.getId()) != null)
			return this.bootDAO.Update(b);
		return false;
	}
	
	public boolean Delete(int b) {
		if (this.bootDAO.getBoot(b) != null)
			return this.bootDAO.Delete(b);
	
		return false;
	}
}
