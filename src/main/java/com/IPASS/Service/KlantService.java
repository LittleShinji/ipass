package com.IPASS.Service;


import java.util.List;

import com.IPASS.model.Klant;
import com.IPASS.model.KlantDAO;

public class KlantService {
	KlantDAO klantenDAO = null;
	
	public KlantService() {
		klantenDAO = new KlantDAO();
	}
	
	public List<Klant> getAll() {
		return this.klantenDAO.getListKlanten();
	}
	
	public Klant Add(Klant k) {
		return this.klantenDAO.Add(k);
	}
	
	public boolean Update(Klant k) {
		if (this.klantenDAO.getKlantByID(k.getId()) != null)
			return this.klantenDAO.Update(k);
		return false;
	}
	
	public boolean Delete(int k) {
		if (this.klantenDAO.getKlantByID(k) != null)
			return this.klantenDAO.Delete(k);
		
		return false;
		
	}
	
	public Klant getKlant(int id) {
		return this.klantenDAO.getKlantByID(id);
	}
	
	public int Count() {
		return this.klantenDAO.Count();
	}
}
