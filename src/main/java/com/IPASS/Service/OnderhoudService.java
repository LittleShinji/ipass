package com.IPASS.Service;


import java.util.List;

import com.IPASS.model.Onderhoud;
import com.IPASS.model.OnderhoudDAO;

public class OnderhoudService {
	private OnderhoudDAO ondhoudDAO;
	
	public OnderhoudService() {
		this.ondhoudDAO = new OnderhoudDAO();
	}
	
	public List<Onderhoud> getAll() {
		return this.ondhoudDAO.getListBoekingen();
	}
	
	public Onderhoud getBootById(int id) {
		return this.ondhoudDAO.getOnderhoudByID(id);
	}
	
	public int Count() {
		return this.ondhoudDAO.Count();
	}
	
	public Onderhoud Add(Onderhoud o) {
		return this.ondhoudDAO.Add(o);
	}
	
	public boolean Update(Onderhoud o) {
		if (this.ondhoudDAO.getOnderhoudByID(o.getId()) != null)
			return this.ondhoudDAO.Update(o);
		return false;
	}
	
	public boolean Delete(int o) {
		if (this.ondhoudDAO.getOnderhoudByID(o) != null)
			return this.ondhoudDAO.Delete(o);
		
		return false;
		
		
	}
}
