package com.IPASS.Service;

import com.IPASS.model.GebruikerDAO;

public class ServiceProvider {
	private static BootService bootService;
	private static BoekingService boekingService;
	private static OnderhoudService onderhoudService;
	private static GebruikerDAO gebruikerDAO;
	private static KlantService klantService;
	
	public static BootService getBootService() {
		if (bootService == null)
			bootService = new BootService();
		return bootService;
	}
	
	public static BoekingService getBoekingService() {
		if (boekingService == null)
			boekingService = new BoekingService();
		return boekingService;
	}
	
	public static OnderhoudService getOnderhoudService() {
		if (onderhoudService == null)
			onderhoudService = new OnderhoudService();
		return onderhoudService;
	}
	
	public static GebruikerDAO getGebruikerDAO() {
		if (gebruikerDAO == null)
			gebruikerDAO = new GebruikerDAO();
		return gebruikerDAO;
	}
	
	public static KlantService getKlantService() {
		if (klantService == null)
			klantService = new KlantService();
		return klantService;
	}
	
}
