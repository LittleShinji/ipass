package com.IPASS.model;


import java.util.Date;

public class Boeking {
	private int id;
	private Date startdatum;
	private Date einddatum;
	private Boot boot;
	private Klant klant;
	
	public Boeking(int id, Date startdatum, Date einddatum, Boot boot, Klant klant) {
		this.id = id;
		this.startdatum = startdatum;
		this.einddatum = einddatum;
		this.boot = boot;
		this.klant = klant;
	}

	public Boeking(Date startdatum, Date einddatum, Boot boot, Klant klant) {
		this.startdatum = startdatum;
		this.einddatum = einddatum;
		this.boot = boot;
		this.klant = klant;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getStartdatum() {
		return startdatum;
	}

	public void setStartdatum(Date startdatum) {
		this.startdatum = startdatum;
	}

	public Date getEinddatum() {
		return einddatum;
	}

	public void setEinddatum(Date einddatum) {
		this.einddatum = einddatum;
	}

	public Boot getBoot() {
		return boot;
	}

	public void setBoot(Boot b) {
		this.boot = b;
	}

	public Klant getKlant() {
		return klant;
	}

	public void setKlant(Klant k) {
		this.klant = k;
	}
	
	
	
}
