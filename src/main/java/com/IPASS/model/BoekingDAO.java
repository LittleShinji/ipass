package com.IPASS.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BoekingDAO extends SQLConnection {

	public BoekingDAO() {
		super();
	}

	public List<Boeking> getListBoekingen() {
		List<Boeking> result = new ArrayList<Boeking>();

		System.out.println("`Boekingen` uit Database");

		String query = "SELECT bn.ID_BOEKING ID_BOEKING, bn.START_DATUM START_DATUM, bn.EIND_DATUM EIND_DATUM, bn.ID_KLANT ID_KLANT, "
				+ "bn.ID_BOOT ID_BOOT, k.NAAM KLANTNAAM, k.ADRES ADRES, k.EMAIL EMAIL, k.WOONPLAATS WOONPLAATS, k.POSTCODE POSTCODE, "
				+ "k.BANKREKENINGNUMMER BANKREKENINGNUMMER, b.NAAM BOOTNAAM, b.PRIJS_PER_DAG PRIJS_PER_DAG, "
				+ "b.BORG BORG, b.VERKOOP_PRIJS VERKOOP_PRIJS, b.STATUS STATUS FROM `Boekingen` bn LEFT JOIN Klanten k ON bn.ID_KLANT = k.id_klant LEFT JOIN `Boten` b ON bn.ID_BOOT = b.ID_BOOT";
		System.out.println(query);
		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {

				int status = 1;
				if (rs.getString("status").equals("Verkoopbaar"))
					status = 2;

				if (rs.getString("Status").equals("Afgeschreven"))
					status = 3;

				Klant klant = new Klant(rs.getInt("id_klant"), rs.getString("klantnaam"), rs.getString("adres"),
						rs.getString("email"), rs.getString("woonplaats"), rs.getString("postcode"),
						rs.getString("Bankrekeningnummer"));
				Boot boot = new Boot(rs.getInt("id_boot"), rs.getString("bootnaam"), rs.getFloat("prijs_per_dag"),
						rs.getFloat("borg"), status, rs.getFloat("verkoop_prijs"));

				Boeking k = new Boeking(rs.getInt("id_boeking"), stringToDate(rs.getString("start_datum")),
						stringToDate(rs.getString("eind_datum")), boot, klant);
				result.add(k);

			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		return result;
	}

	public Boeking getBoekingen(int id) {
		Boeking result = null;

		System.out.println("`Boekingen` uit Database");

		String query = "SELECT bn.ID_BOEKING ID_BOEKING, bn.START_DATUM START_DATUM, bn.EIND_DATUM EIND_DATUM, bn.ID_KLANT ID_KLANT, "
				+ "bn.ID_BOOT ID_BOOT, k.NAAM KLANTNAAM, k.ADRES ADRES, k.EMAIL EMAIL, k.WOONPLAATS WOONPLAATS, k.POSTCODE POSTCODE, "
				+ "k.BANKREKENINGNUMMER BANKREKENINGNUMMER, b.NAAM BOOTNAAM, b.PRIJS_PER_DAG PRIJS_PER_DAG, "
				+ "b.BORG BORG, b.VERKOOP_PRIJS VERKOOP_PRIJS, b.STATUS STATUS FROM `Boekingen` bn LEFT JOIN Klanten k ON bn.ID_KLANT = k.id_klant LEFT JOIN Boten b ON bn.ID_BOOT = b.ID_BOOT "
				+ "WHERE bn.id_boeking = " + id;
		System.out.println(query);
		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				int status = 1;
				if (rs.getString("status").equals("Verkoopbaar"))
					status = 2;

				if (rs.getString("Status").equals("Afgeschreven"))
					status = 3;
				Klant klant = new Klant(rs.getInt("id_klant"), rs.getString("klantnaam"), rs.getString("adres"),
						rs.getString("email"), rs.getString("woonplaats"), rs.getString("postcode"),
						rs.getString("Bankrekeningnummer"));
				Boot boot = new Boot(rs.getInt("id_boot"), rs.getString("bootnaam"), rs.getFloat("prijs_per_dag"),
						rs.getFloat("borg"), status, rs.getFloat("verkoop_prijs"));

				Boeking k = new Boeking(rs.getInt("id_boeking"), stringToDate(rs.getString("start_datum")),
						stringToDate(rs.getString("eind_datum")), boot, klant);
				result = k;
			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		return result;
	}

	private Date stringToDate(String dateInString) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		;
		Date d = null;
		try {

			Date date = formatter.parse(dateInString);
			d = date;
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return d;
	}

	public int Count() {
		int count = 0;
		String query = "SELECT count(*) count FROM `Boekingen`";

		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {

				count = rs.getInt("count");
			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}

		return count;

	}

	public Boeking Add(Boeking b) {
		Boeking result = null;
		boolean update = true;
		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			

			String query = "SELECT count(*) count FROM Klanten WHERE ID_Klant = " + b.getKlant().getId();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				if (rs.getInt("count") != 1)
					update = false;
			}

			query = "SELECT count(*) count FROM Boten WHERE ID_Boot = " + b.getBoot().getId();
			rs = stmt.executeQuery(query);

			while (rs.next()) {
				if (rs.getInt("count") != 1)
					update = false;
			}

			if (update) {
				query = "INSERT INTO `Boekingen` (`ID_Boeking`, `Start_datum`, `Eind_datum`, `ID_Klant`, `ID_Boot`) "
						+ "VALUES (NULL, '" + convertDateToString(b.getStartdatum()) + "', '"
						+ convertDateToString(b.getEinddatum()) + "', '" + b.getKlant().getId() + "', '"
						+ b.getBoot().getId() + "')";
				System.out.print(query);
				stmt.executeUpdate(query);

			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}

		if (update == true) {
			try {
				Connection conn = super.getConnection();
				Statement stmt = conn.createStatement();

				String query = "SELECT ID_Boeking FROM `Boekingen` WHERE Start_datum = '"
						+ convertDateToString(b.getStartdatum()) + "' AND " + "`Eind_datum` = '"
						+ convertDateToString(b.getEinddatum()) + "' AND `ID_Klant` = '" + b.getKlant().getId()
						+ "' AND `ID_Boot` = '" + b.getBoot().getId() + "'";
				System.out.println(query);
				ResultSet rs = stmt.executeQuery(query);

				while (rs.next()) {
						b.setId(rs.getInt("ID_Boeking"));
					result = b;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println(e);
			}
		}

		// return count;

		return result;
	}

	private String convertDateToString(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String format = formatter.format(date);
		System.out.println("To String: " + format);
		return format;
	}

	public boolean Delete(int b) {

		String query = "DELETE FROM `Boekingen` WHERE `Boekingen`.`ID_Boeking` = " + b;

		System.out.println(query);
		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();

			int r = stmt.executeUpdate(query);
			System.out.print("Query out: " + r);
			stmt.close();
			conn.close();

			return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}

		return false;
	}
}
