package com.IPASS.model;


public class Boot {
	private int id;
	private String naam;
	private float prijs_per_dag, borg, verkoop_prijs;
	private int status;
	
	public Boot(int id, String naam, float ppd, float borg, int status, float verkoop_prijs) {
		this.id = id;
		this.naam = naam;
		this.prijs_per_dag = ppd;
		this.borg = borg;
		this.status = status;
		this.verkoop_prijs = verkoop_prijs;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public float getPrijs_per_dag() {
		return prijs_per_dag;
	}

	public void setPrijs_per_dag(float prijs_per_dag) {
		this.prijs_per_dag = prijs_per_dag;
	}

	public float getBorg() {
		return borg;
	}

	public void setBorg(float borg) {
		this.borg = borg;
	}

	public String getStatus() {
		String status = "Verhuurbaar";
		
		if (this.status == 2) {
			status = "Verkoopbaar";
		}
		if (this.status == 3) {
			status = "Afgeschreven";
		}
		
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public void setStatus(String status) {
		this.status = 1;
		if (status == "Verkoopbaar")
			this.status = 2;
		if (status == "Afgeschreven")
			this.status = 3;
	}

	public float getVerkoop_prijs() {
		return verkoop_prijs;
	}

	public void setVerkoop_prijs(float verkoop_prijs) {
		this.verkoop_prijs = verkoop_prijs;
	}
}
