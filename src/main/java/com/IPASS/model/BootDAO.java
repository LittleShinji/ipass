package com.IPASS.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.IPASS.Service.ServiceProvider;

public class BootDAO extends SQLConnection {

	public BootDAO() {
		super();
	}

	public List<Boot> getListBoten() {
		List<Boot> result = new ArrayList<Boot>();

		System.out.println("Boten uit Database");

		String query = "SELECT * FROM Boten";

		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				

				int status = 1;
				if (rs.getString("status").equals("Verkoopbaar"))
					status = 2;
				
				if (rs.getString("Status").equals("Afgeschreven")) 
					status = 3;
				Boot k = new Boot(rs.getInt("id_boot"), rs.getString("naam"), rs.getFloat("prijs_per_dag"),
						rs.getFloat("borg"), status, rs.getFloat("verkoop_prijs"));
				result.add(k);

			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		return result;
	}

	public Boot Add(Boot b) {
		
		Boot result = null;
		
		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			
			String query = "INSERT INTO `Boten` (`ID_Boot`, `Naam`, `Prijs_Per_Dag`, `Borg`, `Verkoop_prijs`, `Status`) "
					+ "VALUES (NULL, '"+b.getNaam()+"', '"+b.getPrijs_per_dag()+"', '"+b.getBorg()+"', '"+b.getVerkoop_prijs()+"', '"+b.getStatus()+"')";
			System.out.println(query);
			stmt.executeUpdate(query);
			query = "SELECT id_boot FROM Boten WHERE `naam` = '"+b.getNaam()+"' AND Prijs_Per_Dag = '"+b.getPrijs_per_dag()+"' AND `Borg` = "
					+ "'" + b.getBorg()+"' AND Verkoop_prijs = '"+b.getVerkoop_prijs()+"' AND Status = '"+b.getStatus()+"'";
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				b.setId(rs.getInt("ID_Boot"));
				result = b;
			}
			conn.close();
		} catch (SQLException e) {
			System.out.println(e);
		}
		return result;
	}

	public boolean Update(Boot b) {
		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			String query = "UPDATE `Boten` SET `Naam` = '"+ b.getNaam() +"', `Prijs_Per_Dag` = '"+ b.getPrijs_per_dag() +"', `Borg` = '" + b.getBorg() + "', "+
					"`Verkoop_prijs` = '"+b.getVerkoop_prijs()+"', `Status` = '"+ b.getStatus() +"' WHERE `Boten`.`ID_Boot` = " + b.getId();
			stmt.executeUpdate(query);

			stmt.close();
			conn.close();
			
			return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		
		return false;
	}

	public boolean Delete(int b) {
		
		boolean succes = true;
		
		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			
			
			String query = "DELETE FROM `Boekingen` WHERE `Boekingen`.`ID_Boot` = " + b;
			stmt.executeUpdate(query);
			System.out.println(query);
			stmt.close();
			conn.close();
			

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
			succes = false;
		}
		
		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			
			
			String query = "DELETE FROM `Onderhoud` WHERE `Onderhoud`.`ID_Boot` = " + b;
			stmt.executeUpdate(query);
			System.out.println(query);
			stmt.close();
			conn.close();
			

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
			succes = false;
		}
		
		if (succes == true) {
		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			
			
			String query = "DELETE FROM `Boten` WHERE `Boten`.`ID_Boot` = " + b;
			stmt.executeUpdate(query);
			System.out.println(query);
			stmt.close();
			conn.close();
			
			return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
			succes = false;
		}
		}
		return succes;
	}

	public int Count() {
		int count = 0;
		String query = "SELECT count(*) count FROM Boten";
		
		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				count = rs.getInt("count");
			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		
		return count;
	}

	public int Countsabale() {
		int count = 0;
		String query = "SELECT count(*) count FROM Boten WHERE status = 2";
		
		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				
				count = rs.getInt("count");
			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		
		return count;
	}

	public int Countleasable() {
		int count = 0;
		String query = "SELECT count(*) count FROM Boten WHERE status = 1";
		
		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				
				count = rs.getInt("count");
			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		
		return count;
	}

	public Boot getBoot(int id) {
			Boot result = null;

			System.out.println("Boten uit Database");

			String query = "SELECT * FROM Boten WHERE id_boot = " + id;

			try {
				Connection conn = super.getConnection();
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(query);

				while (rs.next()) {
					
					int status = 1;
					if (rs.getString("status").equals("Verkoopbaar"))
						status = 2;
					
					if (rs.getString("Status").equals("Afgeschreven")) 
						status = 3;
					
					Boot k = new Boot(rs.getInt("id_boot"), rs.getString("naam"), rs.getFloat("prijs_per_dag"),
							rs.getFloat("borg"), status, rs.getFloat("verkoop_prijs"));
					result= k;

				}

				stmt.close();
				conn.close();

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println(e);
			}
			return result;
	}
}
