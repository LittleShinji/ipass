package com.IPASS.model;


public class Gebruiker {
	private String naam, password;
	
	public Gebruiker(String nm, String pw) {
		this.naam = nm;
		this.password = pw;
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public boolean getPassword(String pass) {
		return this.password.equals(pass);
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
