package com.IPASS.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GebruikerDAO extends SQLConnection {
	public GebruikerDAO() {
		super();
	}

	public List<Gebruiker> getAll() {
		List<Gebruiker> result = new ArrayList<Gebruiker>();

		System.out.println("Gebruikers uit database");

		String query = "SELECT * FROM Gebruikers";

		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				Gebruiker g = new Gebruiker(rs.getString(1), rs.getString(2));
				result.add(g);
			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}

		return result;
	}

	public boolean checkLogin(String pass) {

		for (Gebruiker item : getAll()) {
			if (item.getPassword(pass))
				return true;
		}

		return false;
	}
}
