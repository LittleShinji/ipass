package com.IPASS.model;


public class Klant {
	private int id;
	private String naam, adres, email, woonplaats, postcode, Bankrekeningnummer;
	
	public Klant(int id, String naam, String adres, String email, String woonplaats, String postcode, String Bankrekeningnummer) {
		this.id = id;
		this.naam = naam;
		this.adres = adres;
		this.email = email;
		this.woonplaats = woonplaats;
		this.postcode = postcode;
		this.Bankrekeningnummer = Bankrekeningnummer;
	}
	
	public Klant(String naam, String adres, String email, String woonplaats, String postcode, String Bankrekeningnummer) {
		this.naam = naam;
		this.adres = adres;
		this.email = email;
		this.woonplaats = woonplaats;
		this.postcode = postcode;
		this.Bankrekeningnummer = Bankrekeningnummer;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaam() {
		return naam;
	}

	public void setNaam(String naam) {
		this.naam = naam;
	}

	public String getAdres() {
		return adres;
	}

	public void setAdres(String adres) {
		this.adres = adres;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWoonplaats() {
		return woonplaats;
	}

	public void setWoonplaats(String woonplaats) {
		this.woonplaats = woonplaats;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getBankrekeningnummer() {
		return Bankrekeningnummer;
	}

	public void setBankrekeningnummer(String bankrekeningnummer) {
		Bankrekeningnummer = bankrekeningnummer;
	}
	
	
}
