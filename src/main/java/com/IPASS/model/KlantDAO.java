package com.IPASS.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public class KlantDAO extends SQLConnection {
	public KlantDAO() {
		super();
	}

	public List<Klant> getListKlanten() {
		List<Klant> result = new ArrayList<Klant>();

		System.out.println("Klanten uit Database");

		String query = "SELECT * FROM Klanten";

		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				Klant k = new Klant(rs.getInt("id_klant"), rs.getString("naam"), rs.getString("adres"),
						rs.getString("email"), rs.getString("woonplaats"), rs.getString("postcode"),
						rs.getString("bankrekeningnummer"));
				result.add(k);

			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		return result;
	}

	public List<Klant> getListKlanten(int page) {
		List<Klant> result = new ArrayList<Klant>();

		int startrecord = ((page - 1) * 20) + 1;
		int lastrecord = (page - 1) * 20 + 20;

		System.out.println("Klanten Page uit Database");

		try {
			String query = "SELECT * FROM ( SELECT ROWNUM as rw, ID_KLANT ,NAAM ,ADRES ,EMAIL ,WOONPLAATS ,POSTCODE , "
					+ "BANKREKENINGNUMMER FROM Klanten) WHERE rw BETWEEN " + startrecord + " AND " + lastrecord;
			System.out.println(query);

			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				Klant k = new Klant(rs.getInt("id_klant"), rs.getString("naam"), rs.getString("adres"),
						rs.getString("email"), rs.getString("woonplaats"), rs.getString("postcode"),
						rs.getString("bankrekeningnummer"));
				result.add(k);

			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}

		return result;
	}

	public Klant getKlantByID(int i) {
		Klant result = null;

		System.out.println("Klanten uit Database");

		String query = "SELECT * FROM Klanten";

		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				if (rs.getInt("id_klant") == i) {
				result = new Klant(rs.getInt("id_klant"), rs.getString("naam"), rs.getString("adres"),
						rs.getString("email"), rs.getString("woonplaats"), rs.getString("postcode"),
						rs.getString("bankrekeningnummer"));
				}
			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}

		return result;
	}

	public Klant Add(Klant k) {
		Klant result = null;
		
		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			
			String query = "INSERT INTO `Klanten` (`ID_Klant`, `Naam`, `Adres`, `Email`, "+
					"`Woonplaats`, `Postcode`, `Bankrekeningnummer`) VALUES (NULL, '"+k.getNaam()+"', '"+k.getAdres()+"',"
					+ " '"+k.getEmail()+"', '"+k.getWoonplaats()+"', '"+k.getPostcode() +"', '"+k.getBankrekeningnummer()+"')";
			stmt.executeUpdate(query);
			query = "SELECT id_klant FROM Klanten WHERE `naam` = '"+k.getNaam()+"' AND adres = '"+k.getAdres()+"' AND `email` = "
					+ "'" + k.getEmail()+"' AND woonplaats = '"+k.getWoonplaats()+"' AND postcode = '"+k.getPostcode() +"' AND "
							+ "bankrekeningnummer = '"+k.getBankrekeningnummer()+"'";
			ResultSet rs = stmt.executeQuery(query);
			
			while (rs.next()) {
				k.setId(rs.getInt("ID_Klant"));
				result = k;
			}
			conn.close();
		} catch (SQLException e) {
			System.out.println(e);
		}
		return result;
	}

	public boolean Update(Klant k) {

		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			String query = "UPDATE `Klanten` SET `Naam` = '"+k.getNaam()+"', `Adres` = '"+k.getAdres()+"', "+
					"`Email` = '"+k.getEmail()+"', `Woonplaats` = '"+k.getWoonplaats()+"', `Postcode` = '"+k.getPostcode()+"', "+
					"`Bankrekeningnummer` = '"+k.getBankrekeningnummer()+"' WHERE `Klanten`.`ID_Klant` = " + k.getId();
			stmt.executeUpdate(query);

			stmt.close();
			conn.close();
			
			return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		
		return false;
	}

	public boolean Delete(int b) {

		boolean succes = true;
		
		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			
			
			String query = "DELETE FROM `Boekingen` WHERE `Boekingen`.`ID_Klant` = " + b;
			stmt.executeUpdate(query);
			System.out.println(query);
			stmt.close();
			conn.close();
			

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
			succes = false;
		}
		
		if (succes == true) {
		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			
			
			String query = "DELETE FROM `Klanten` WHERE `ID_Klant` = " + b;
			stmt.executeUpdate(query);
			System.out.println(query);
			stmt.close();
			conn.close();
			
			return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
			succes = false;
		}
		}
		return succes;
	}

	public int Count() {
		int count = 0;

		System.out.println("Klanten uit Database");

		String query = "SELECT count(*) count FROM Klanten";

		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				count = rs.getInt("count");

			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}

		return count;
	}

}
