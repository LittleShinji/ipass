package com.IPASS.model;


import java.util.Date;

public class Onderhoud {
	private int id;
	private Date startdatum;
	private Date einddatum;
	private Boot boot;
	
	public Onderhoud(int id, Date startdatum, Date einddatum, Boot boot) {
		this.id = id;
		this.startdatum = startdatum;
		this.einddatum = einddatum;
		this.boot = boot;
	}

	public Onderhoud(Date startdatum, Date einddatum, Boot boot) {
		this.startdatum = startdatum;
		this.einddatum = einddatum;
		this.boot = boot;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getStartdatum() {
		return startdatum;
	}

	public void setStartdatum(Date startdatum) {
		this.startdatum = startdatum;
	}

	public Date getEinddatum() {
		return einddatum;
	}

	public void setEinddatum(Date einddatum) {
		this.einddatum = einddatum;
	}

	public Boot getBoot() {
		return boot;
	}

	public void setBoot(Boot b) {
		this.boot = b;
	}
	
}
