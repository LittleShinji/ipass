package com.IPASS.model;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.IPASS.Service.ServiceProvider;

public class OnderhoudDAO extends SQLConnection {
	
	public OnderhoudDAO() {
		super();
	}
	
	public List<Onderhoud> getListBoekingen() {
		List<Onderhoud> result = new ArrayList<Onderhoud>();

		System.out.println("Onderhoud uit Database");

		String query = "SELECT * FROM Onderhoud";

		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				Onderhoud k = new Onderhoud(rs.getInt("id_Onderhoud"), 
						rs.getDate("start_datum"), 
						rs.getDate("eind_datum"), 
						ServiceProvider.getBootService().getBootById(rs.getInt("id_boot")));
				result.add(k);
			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		return result;
	}
	
	public Onderhoud getOnderhoudByID(int i) {
		Onderhoud result = null;
		System.out.println("Onderhoud uit Database");

		String query = "SELECT * FROM Onderhoud WHERE id_Onderhoud = " + i;

		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				Onderhoud k = new Onderhoud(rs.getInt("id_Onderhoud"), 
						rs.getDate("start_datum"), 
						rs.getDate("eind_datum"), 
						ServiceProvider.getBootService().getBootById(rs.getInt("id_boot")));
				result = k;
			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		return result;
	}

	public Onderhoud Add(Onderhoud b) {
		Onderhoud result = null;

		boolean update = true;
		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
		

			String query = "SELECT count(*) count FROM Boten WHERE ID_Boot = " + b.getBoot().getId();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				if (rs.getInt("count") != 1)
					update = false;
			}

			if (update) {
				query = "INSERT INTO `Onderhoud` (`ID_Onderhoud`, `Start_datum`, `Eind_datum`, `ID_Boot`) "
						+ "VALUES (NULL, '" + convertDateToString(b.getStartdatum()) + "', '"
						+ convertDateToString(b.getEinddatum()) + "', '"
						+ b.getBoot().getId() + "')";
				System.out.print(query);
				stmt.executeUpdate(query);

			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}

		if (update == true) {
			try {
				Connection conn = super.getConnection();
				Statement stmt = conn.createStatement();

				String query = "SELECT ID_Onderhoud FROM Onderhoud WHERE Start_datum = '"
						+ convertDateToString(b.getStartdatum()) + "' AND " + "`Eind_datum` = '"
						+ convertDateToString(b.getEinddatum()) + "' AND `ID_Boot` = '" + b.getBoot().getId() + "'";
				System.out.println(query);
				ResultSet rs = stmt.executeQuery(query);

				while (rs.next()) {
						b.setId(rs.getInt("ID_Onderhoud"));
					result = b;
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println(e);
			}
		}
		
		return result;
	}
	
	public boolean Update(Onderhoud b) {
		return true;
	}
	
	public boolean Delete(int b) {

		boolean succes = true;

		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			
			
			String query = "DELETE FROM `Onderhoud` WHERE `Onderhoud`.`ID_Onderhoud` = " + b;
			stmt.executeUpdate(query);
			System.out.println(query);
			stmt.close();
			conn.close();
			
			return true;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
			succes = false;
		}

		return succes;
	}

	private String convertDateToString(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String format = formatter.format(date);
		System.out.println("To String: " + format);
		return format;
	}
	
	public int Count() {
		int count = 0;
		System.out.println("Onderhoud uit Database");

		String query = "SELECT count(*) count FROM Onderhoud";

		try {
			Connection conn = super.getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				
				count = rs.getInt("count");
			}

			stmt.close();
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);
		}
		
		return count;
	}
}
