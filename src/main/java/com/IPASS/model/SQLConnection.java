package com.IPASS.model;

import java.sql.Connection;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class SQLConnection {

	/*
	 * Root User: adminuEzJUZ3 Root Password: w-HASRdI-Yhp URL:
	 * https://ipass-dannyboevee.rhcloud.com/phpmyadmin/
	 */
	public SQLConnection() {

	}

	public Connection getConnection() {

		Connection result = null;
		try {
			InitialContext ic = new InitialContext();
			DataSource ds = (DataSource) ic.lookup("java:comp/env/jdbc/MySQLDS");

			result = ds.getConnection();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		return result;
	}
}
