<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${sessionScope.LoggedUser == null}">
	<%@include file="login.jsp" %>
</c:if>
<c:if test="${sessionScope.LoggedUser != null}">
	<c:redirect url="/admin/"/>
</c:if>