<html>
<head>
<title>Piet's botenverhuur</title>
<link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<!--Import materialize.css-->
<link type="text/css" rel="stylesheet"
	href="materialize/css/materialize.min.css" media="screen,projection" />
<!--Let browser know website is optimized for mobile-->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link type="text/css" rel="stylesheet" href="css/login.css" />
</head>
<body>
	<div class="container login-wrapper valign-wrapper">
		<div class="main-body">

			<div class="card medium login hoverable" style="height: auto;">
				<div class="row" style="margin-bottom: 0px;">
					<form class="col s12" autocomplete="off" method="post" id="login"
						action="loginservlet.do" style="margin-bottom: 0px;">
						<div class="row">
							<div class="input-field col s12 center-align">
								<img class="images-login" src="images/pietsbotenverhuur.png" />
							</div>
						</div>
						<c:if test="${not empty error}">
							<div class="row" style="margin-bottom: 0px;">
								<div class="col s12" style="color: red; margin-bottom: 0px;">
									${error}</div>
							</div>
						</c:if>

						<div class="row">
							<div class="input-field col s12">
								<input name="username" id="username" type="text" value="${username}"
									class="validate"> <label for="username">Gebruikersnaam</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<input id="password" name="password" type="password"
									class="validate"> <label for="password">Wachtwoord</label>
							</div>
						</div>

						<div class="row">
							<div class="col s6 left-align">
								<input type="checkbox" name="remember" id="remember" /> <label
									for="remember">Onthoud mij</label>
							</div>
							<div class="col s6 right-align">

								<button class="waves-effect waves-light btn  grey darken-2"
									type="submit" form="login">Login</button>
							</div>
						</div>
					</form>
				</div>

			</div>
			<div class="forgetpassword center-align">
				<a class="a-forgot" href="loginServlet.do">Wachtwoord vergeten</a>
			</div>
		</div>

		<script type="text/javascript"
			src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
		<script type="text/javascript" src="materialize/js/materialize.min.js"></script>
		
		<script>

		function getCookie(cname) {
			var name = cname + "=";
			var ca = document.cookie.split(';');
			for (var i = 0; i < ca.length; i++) {
				var c = ca[i].trim();
				if (c.indexOf(name) == 0)
					return c.substring(name.length, c.length);
			}
			return "";
		}

		if (username = getCookie("loggedUsername")) {
			if (password = getCookie("loggedPassword")) {
				$("#username").val(username);
				$("#password").val(password);
				$("#remember").prop('checked', true);
			}
		}
	</script>
</body>
</html>