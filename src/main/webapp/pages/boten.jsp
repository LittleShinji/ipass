<%@ page import="com.IPASS.model.*"%>
<%@ page import="com.IPASS.Service.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="col s12">
	<div class="panel">

		<table class="bordered table_pagination">
			<thead>
				<tr>
					<td>ID</td>
					<td>Naam</td>
					<td class="hide-on-med-and-down">Prijs per dag</td>
					<td class="hide-on-med-and-down">Borg</td>
					<td>Status</td>
					<td></td>
				</tr>
			</thead>
			<tbody>

				<c:forEach var="item"
					items="${ServiceProvider.getBootService().getAll()}">
					<tr class="tr-row" id="${item.id}">
						<td>${item.id}</td>
						<td id="name")>${item.naam}</td>
						<td id="ppd" class="hide-on-med-and-down">&euro;
							${item.prijs_per_dag}</td>
						<td id="ppd" class="hide-on-med-and-down">&euro; ${item.borg}</td>

						<td id="status">${item.status}</td>
						<td class="right"><a
							class="show btn-floating green hide-on-small-only"
							data-id="${item.id}" data-name="${item.naam}"
							data-prijsperdag="${item.prijs_per_dag}" data-borg="${item.borg}"
							data-verkoopprijs="${item.verkoop_prijs}"
							data-status="${item.status}"><i
								class="material-icons" style="color: white;">visibility</i></a><a
							class="edit btn-floating yellow hide-on-small-only"
							data-id="${item.id}" data-name="${item.naam}"
							data-prijsperdag="${item.prijs_per_dag}" data-borg="${item.borg}"
							data-verkoopprijs="${item.verkoop_prijs}"
							data-status="${item.status}"><i class="material-icons"
								style="color: white;">mode_edit</i></a><a
							class="delete btn-floating red hide-on-small-only"
							data-id="${item.id}" data-name="${item.naam}"
							data-prijsperdag="${item.prijs_per_dag}" data-borg="${item.borg}"
							data-verkoopprijs="${item.verkoop_prijs}"
							data-status="${item.status}"><i class="material-icons"
								style="color: white;">delete</i></a>

							<div
								class="button-table right fixed-action-btn horizontal click-to-toggle hide-on-med-and-up">
								<a class="btn-floating" style="background-color: #262B33;">
									<i class="material-icons">menu</i>
								</a>
								<ul>
									<li class="button-li"><a class="show btn-floating green"
										data-id="${item.id}"><i class="material-icons"
											style="color: white;">visibility</i></a></li>
									<li class="button-li"><a class="edit btn-floating yellow"
										href="#edit"><i class="material-icons"
											style="color: white;">mode_edit</i></a></li>
									<li class="button-li"><a class="delete btn-floating red"
										href="#delete"><i class="material-icons"
											style="color: white;">delete</i></a></li>
								</ul>
							</div></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<ul class="pagination table_pagina center">
		</ul>
	</div>
</div>
<div class="fixed-action-btn" style="bottom: 45px; right: auto;">
	<a class="add btn-floating btn-large black"> <i
		class="large material-icons">add</i>
	</a>
</div>

<%@include file="/pages/modals/boten.jsp"%>