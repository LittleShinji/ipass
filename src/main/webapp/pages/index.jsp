<%@ page import="com.IPASS.Service.*"%>
<div class="row">
	<div class="col s12 m3">
		<div class="panel panel-box hoverable">
			<div class="panel-top">
				<span><i class="medium material-icons" style="color: green;">event</i></span>
			</div>
			<div class="panel-info">
				<span class="ng-scope"">Aantal Boekingen</span>
			</div>
			<div class="panel-bottom">
				<span>${ServiceProvider.getBoekingService().Count()}</span>
			</div>
		</div>
	</div>
	<div class="col s12 m3">
		<div class="panel panel-box hoverable">
			<div class="panel-top">
				<span><i class="medium material-icons" style="color: green;">perm_identity</i></span>
			</div>
			<div class="panel-info">
				<span class="ng-scope"">Aantal Klanten</span>
			</div>
			<div class="panel-bottom">
				<span>${ServiceProvider.getKlantService().Count()}</span>
			</div>
		</div>
	</div>
	<div class="col s12 m3">
		<div class="panel panel-box hoverable">
			<div class="panel-top">
				<span><i class="medium material-icons" style="color: green;">directions_boat</i></span>
			</div>
			<div class="panel-info">
				<span class="ng-scope"">Aantal boten</span>
			</div>
			<div class="panel-bottom">
				<span>${ServiceProvider.getBootService().Count()}</span>
			</div>
		</div>
	</div>
	<div class="col s12 m3">
		<div class="panel panel-box hoverable">
			<div class="panel-top">
				<span><i class="medium material-icons" style="color: green;">build</i></span>
			</div>
			<div class="panel-info">
				<span class="ng-scope"">Aantal onderhouds boekingen</span>
			</div>
			<div class="panel-bottom">
				<span>${ServiceProvider.getOnderhoudService().Count()}</span>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col s12 m3">
		<div class="panel panel-box hoverable">
			<div class="panel-top">
				<span><i class="medium material-icons" style="color: green;">event</i></span>
			</div>
			<div class="panel-info">
				<span class="ng-scope"">Af te halen Boekingen</span>
			</div>
			<div class="panel-bottom">
				<span>${ServiceProvider.getBoekingService().PickUpBookings()}</span>
			</div>
		</div>
	</div>
	<div class="col s12 m3">
		<div class="panel panel-box hoverable">
			<div class="panel-top">
				<span><i class="medium material-icons" style="color: green;">event</i></span>
			</div>
			<div class="panel-info">
				<span class="ng-scope"">terug te brengen boekingen</span>
			</div>
			<div class="panel-bottom">
				<span>${ServiceProvider.getBoekingService().TakeBackBookings()}</span>
			</div>
		</div>
	</div>
	<div class="col s12 m3">
		<div class="panel panel-box hoverable">
			<div class="panel-top">
				<span><i class="medium material-icons" style="color: green;">directions_boat</i></span>
			</div>
			<div class="panel-info">
				<span class="ng-scope"">Aantal verhuurbare boten</span>
			</div>
			<div class="panel-bottom">
				<span>${ServiceProvider.getBootService().Countleasable()}</span>
			</div>
		</div>
	</div>
	<div class="col s12 m3">
		<div class="panel panel-box hoverable">
			<div class="panel-top">
				<span><i class="medium material-icons" style="color: green;">directions_boat</i></span>
			</div>
			<div class="panel-info">
				<span class="ng-scope"">Aantal verkoopbare boren</span>
			</div>
			<div class="panel-bottom">
				<span>${ServiceProvider.getBootService().Countsalable()}</span>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col s12 m3">
		<div class="panel panel-box hoverable">
			<div class="panel-top">
				<span><i class="medium material-icons" style="color: green;">euro_symbol</i></span>
			</div>
			<div class="panel-info">
				<span class="ng-scope"">Omzet deze maand</span>
			</div>
			<div class="panel-bottom">
				<span><span class='size-h4'>&euro;</span> ${ServiceProvider.getBoekingService().ThisMonthRevenue()}</span>
			</div>
		</div>
	</div>
	<div class="col s12 m3">
		<div class="panel panel-box hoverable">
			<div class="panel-top">
				<span><i class="medium material-icons" style="color: green;">euro_symbol</i></span>
			</div>
			<div class="panel-info">
				<span class="ng-scope"">Totale Omzet</span>
			</div>
			<div class="panel-bottom">
				<span><span class='size-h4'>&euro;</span> ${ServiceProvider.getBoekingService().TotalRevenue()}</span>
			</div>
		</div>
	</div>
	<div class="col s12 m3">
		<div class="panel panel-box hoverable">
			<div class="panel-top">
				<span><i class="medium material-icons" style="color: green;">euro_symbol</i></span>
			</div>
			<div class="panel-info">
				<span class="ng-scope"">Verwachte omzet deze maand</span>
			</div>
			<div class="panel-bottom">
				<span><span class='size-h4'>&euro;</span> ${ServiceProvider.getBoekingService().ExpectedRevenueThisMonth()}</span>
			</div>
		</div>
	</div>
	<div class="col s12 m3">
		<div class="panel panel-box hoverable">
			<div class="panel-top">
				<span><i class="medium material-icons" style="color: green;">euro_symbol</i></span>
			</div>
			<div class="panel-info">
				<span class="ng-scope"">Verwachte omzet volgende maand</span>
			</div>
			<div class="panel-bottom">
				<span><span class='size-h4'>&euro;</span> ${ServiceProvider.getBoekingService().ExpectedRevenueNextMonth()}</span>
			</div>
		</div>
	</div>
</div>