<div id="show" class="modal modal-fixed-footer">
	<div class="modal-content">
		<div class="row">
		<div class="col s6 show-klant">
			<h4>Klant informatie</h4>

			<table class="show-klant">
				<tr>
					<td>Klantnummer:</td>
					<td style="width: 40%;" class="id"></td>
				</tr>
				<tr>
					<td>Naam:</td>
					<td class="name"></td>
				</tr>
				<tr>
					<td>Email:</td>
					<td class="email"></td>
				</tr>
				<tr>
					<td>Adres:</td>
					<td class="adres"></td>
				</tr>
				<tr>
					<td>Woonplaats:</td>
					<td class="city"></td>
				</tr>
				<tr>
					<td>postcode:</td>
					<td class="zipcode"></td>
				</tr>
				<tr>
					<td>Bankrekening:</td>
					<td class="iban"></td>
				</tr>
			</table>
		</div>
		<div class="col s6 show-boot">
			<h4>Boot informatie</h4>
			<table class="show-boot">
				<tr>
					<td>Bootnummer:</td>
					<td class="id"></td>
				</tr>
				<tr>
					<td>Naam:</td>
					<td class="name"></td>
				</tr>
				<tr>
					<td>Prijs per dag:</td>
					<td class="prijsperdag"></td>
				</tr>
				<tr>
					<td>Borg:</td>
					<td class="borg"></td>
				</tr>
			</table>
		</div></div>
		<div class="col s12 show-boeking">
			<h4>Boeking informatie</h4>
			<table class="show-boeking">
				<tr>
					<td>Begin datum:</td>
					<td class="startdate"></td>
				</tr>
				<tr>
					<td>Eind datum</td>
					<td class="enddate"></td>
				</tr>
				<tr>
					<td>Prijs</td>
					<td class="price"></td>
				</tr>
				<tr>
					<td>Borg</td>
					<td class="borg"></td>
				</tr>
				<tr>
					<td>Totaal</td>
					<td class="total"></td>
				</tr>
			</table>
		</div>
	</div>
	<div class="modal-footer">
		<a class="modal-action modal-close waves-effect waves-green btn-flat ">Sluit</a>
	</div>
</div>


<div id="delete" class="div-delete modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Verwijderen</h4>
		<p>Weet u het zeker ?</p>
	</div>
	<div class="modal-footer">
		<input class="data-delete" type="hidden" value="&nbsp;"> <a
			class="modal-action modal-close waves-effect waves-green btn-flat ">Nee</a>
		<a class="modal-delete waves-effect waves-green btn-flat">Ja</a>
	</div>
</div>

<div id="add" class="modal modal-fixed-footer">
	<div class="modal-content">
		<div class="row">
			<div class="col s12">
				<ul class="modal_tabs">
					<li data-page="1" class="tab col s3 chooseboat active"><a>1.
							Kies uw boot</a></li>
					<li data-page="2" class="tab col s3 choosecustomer disabled"><a>2.
							Kies uw klant</a></li>
					<li data-page="3" class="tab col s3 choosedate disabled"><a>3.
							Kies uw datum</a></li>
					<li data-page="4" class="tab col s3 confirmation disabled"><a>Bevestiging</a></li>
				</ul>
			</div>
			<div class="col s12 tab_modal chooseboat">
				<table class="bordered boot">
					<thead>
						<tr>
							<td>Naam</td>
							<td>Prijs per dag</td>
							<td>Borg</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item"
							items="${ServiceProvider.getBootService().getAll()}">
							<tr data-id="${item.id}" data-naam="${item.naam}"
								data-prijsperdag="${item.prijs_per_dag}"
								data-borg="${item.borg}">
								<td>${item.naam}</td>
								<td>${item.prijs_per_dag}</td>
								<td>&euro; ${item.borg}</td>
							</tr>

						</c:forEach>
					</tbody>
				</table>
				<ul class="pagination boot_pagination center"></ul>

			</div>

			<div class="col s12 tab_modal choosecustomer">
				<table class="bordered klant">
					<thead>
						<tr>
							<td>Naam</td>
							<td>Email</td>
							<td>Woonplaats</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="item"
							items="${ServiceProvider.getKlantService().getAll()}">
							<tr data-id="${item.id}" data-name="${item.naam}"
								data-email="${item.email}" data-city="${item.woonplaats}"
								data-adres="${item.adres}" data-zipcode="${item.postcode}"
								data-iban="${item.bankrekeningnummer}">
								<td>${item.naam}</td>
								<td>${item.email}</td>
								<td>${item.woonplaats}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<ul class="pagination klant_pagination center">
			</div>

			<div class="col s12 tab_modal choosedate">
				<div class="agenda">
					<div class="header">
						<div class="col s3" style="text-align: left;">
							<a class="modal-action waves-effect waves-green btn-flat links"><</a>
						</div>
						<div class="col s6 month" style="text-align: center;">Mei</div>
						<div class="col s3" style="text-align: right;">
							<a class="modal-action waves-effect waves-green btn-flat rechts">></a>
						</div>

					</div>
					<div class="days"></div>
				</div>
			</div>

			<div class="col s12 tab_modal confirmation">
				<div class="col s6 data-klant">
					<h4>Klant informatie</h4>

					<table class="data-klant">
						<tr>
							<td>Klantnummer:</td>
							<td style="width: 40%;" class="id"></td>
						</tr>
						<tr>
							<td>Naam:</td>
							<td class="name"></td>
						</tr>
						<tr>
							<td>Email:</td>
							<td class="email"></td>
						</tr>
						<tr>
							<td>Adres:</td>
							<td class="adres"></td>
						</tr>
						<tr>
							<td>Woonplaats:</td>
							<td class="city"></td>
						</tr>
						<tr>
							<td>postcode:</td>
							<td class="zipcode"></td>
						</tr>
						<tr>
							<td>Bankrekening:</td>
							<td class="iban"></td>
						</tr>
					</table>
				</div>
				<div class="col s6 data-boot">
					<h4>Boot informatie</h4>
					<table class="data-boot">
						<tr>
							<td>Bootnummer:</td>
							<td class="id"></td>
						</tr>
						<tr>
							<td>Naam:</td>
							<td class="name"></td>
						</tr>
						<tr>
							<td>Prijs per dag:</td>
							<td class="prijsperdag"></td>
						</tr>
						<tr>
							<td>Borg:</td>
							<td class="borg"></td>
						</tr>
					</table>
				</div>
				<div class="col s12 data-boot">
					<h4>Boot informatie</h4>
					<table class="data-boeking">
						<tr>
							<td>Begin datum:</td>
							<td class="startdate"></td>
						</tr>
						<tr>
							<td>Eind datum</td>
							<td class="enddate"></td>
						</tr>
						<tr>
							<td>Prijs</td>
							<td class="price"></td>
						</tr>
						<tr>
							<td>Borg</td>
							<td class="borg"></td>
						</tr>
						<tr>
							<td>Totaal</td>
							<td class="total"></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<a class="modal-action modal-close btn-flat ">Annuleren</a> <a
			class="button-boeking btn-flat disabled next" data-page="1">Volgende</a>
		<a class="btn-flat disabled previous" data-page="1">Vorige</a>
	</div>
</div>

<script src="/pages/modals/js/boeking.js"></script>
<script>
	$(document).ready(function() {

	});
</script>