<div id="show" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Boot gegevens</h4>
		<div class="modal-show"></div>
	</div>
	<div class="modal-footer">
		<a class="modal-action modal-close waves-effect waves-green btn-flat ">Sluit</a>
	</div>
</div>

<div id="edit" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Boot gegevens</h4>
		<div class="row">
			<form class="col s12 edit-form">
				<div class="row">
					<div class="input-field col s12">
						<input disabled type="text" class="validate id" value="&nbsp;">
						<label for="disabled">Boot nummer</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="name" type="text" class="validate name" value="&nbsp;">
						<label id="label_name" class="active" for="name">Naam</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="email" type="number" step="0.01" class="validate prijsperdag"
							value="&nbsp;"> <label class="active" id="label_email" for="email">Prijs
							per dag</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="adres" type="number" step="0.01" class="validate borg" value="&nbsp;">
						<label id="label_adres" class="active" for="adres">Borg</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="city" type="number" step="0.01" class="validate verkoopprijs"
							value="&nbsp;"> <label id="label_city" for="city">Verkoop
							prijs</label>
					</div>
				</div>
				<!-- <div class="row">
					<div class="input-field col s12">
						<input id="zipcode" type="text" class="validate status"
							value="&nbsp;"> <label id="label_zipcode" for="zipcode">Status</label>
					</div>
				</div> -->
				<div class="row">
					<div class="input-field col s12">
						<select>
							<option value="1">Verhuurbaar</option>
							<option value="2">Verkoopbaar</option>
							<option value="3">Afgeschreven</option>
						</select> <label>Status</label>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="modal-footer">
		<a class="modal-action modal-close waves-effect waves-green btn-flat ">Annuleren</a>
		<a class="modal-edit waves-effect waves-green btn-flat ">Opslaan</a>
	</div>
</div>

<div id="delete" class="div-delete modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Verwijderen</h4>
		<p>Weet u het zeker ?</p>
	</div>
	<div class="modal-footer">
		<input class="data-delete" type="hidden" value="&nbsp;"> <a
			class="modal-action modal-close waves-effect waves-green btn-flat ">Nee</a>
		<a class="modal-delete waves-effect waves-green btn-flat">Ja</a>
	</div>
</div>

<div id="add" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Boot gegevens</h4>
		<div class="row">
			<form class="col s12 add-form">
				<div class="row">
					<div class="input-field col s12">
						<input disabled type="number" class="validate id" value="&nbsp;">
						<label for="disabled">Boot nummer</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="name" type="text" class="validate name" value="&nbsp;">
						<label id="label_name" for="name">Naam</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="prijsperdag" type="number" class="validate prijsperdag"
							value="&nbsp;"> <label id="label_prijs" for="prijsperdag">Prijs
							per dag</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="borg" type="number" class="validate borg"
							value="&nbsp;"> <label id="label_adres" for="borg">Borg</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="verkoopprijs" type="number" class="validate verkoopprijs" value="">
						<label id="label_city" for="verkoopprijs">Verkoop prijs</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<select>
							<option value="1">Verhuurbaar</option>
							<option value="2">Verkoopbaar</option>
							<option value="3">Afgeschreven</option>
						</select> <label>Status</label>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="modal-footer">
		<a class="modal-action modal-close waves-effect waves-green btn-flat ">Annuleren</a>
		<a class="modal-add waves-effect waves-green btn-flat">Opslaan</a>
	</div>
</div>

<script src="/pages/modals/js/boot.js"></script>