$(document).ready(
		
		function() {
			
			$('select').material_select();
		
			
			function checkEmpty(field, table, min) {
				if ($('form.'+table+'-form>.row>.input-field>.' + field).val().length <= min) {
					$('form.'+table+'-form>.row>.input-field>#label_'  + field).attr('data-error', 'Verplicht');
					$('form.'+table+'-form>.row>.input-field>.' + field).removeClass('valid');
					$('form.'+table+'-form>.row>.input-field>.' + field).addClass('invalid'); 
					return false;
				}
				return true;
			}
			
			function checkMax(field, table, max) {
				if ($('form.'+table+'-form>.row>.input-field>.' + field).val().length > max) {
					$('form.'+table+'-form>.row>.input-field>#label_'  + field).attr('data-error', 'Maximale karacters: ' + max);
					$('form.'+table+'-form>.row>.input-field>.' + field).removeClass('valid');
					$('form.'+table+'-form>.row>.input-field>.' + field).addClass('invalid'); 
					return false;
				}
				return true;
			}
			
			function checkField(field, table, max, min) {
				var update = true;
				if (checkEmpty(field, table, min) === false)
					update = false;
				if (checkMax(field, table, max) === false)
					update = false;
				return update;
			}
			
			// Start Modal Show
			$(document).on(
					'click',
					'.show',
					function() {
						
						$('.modal-show').empty();
						$('.modal-show')
								.append(
										"Boot nummer: " + $(this).data('id')
												+ "</br>");
						$('.modal-show').append(
								"Naam: " + $(this).data('name') + "</br>");
						$('.modal-show').append(
								"Prijs per dag: &euro; " + $(this).data('prijsperdag') + "</br>");
						$('.modal-show').append(
								"Borg: &euro;" + $(this).data('borg') + "</br>");
						$('.modal-show').append(
								"Verkoopprijs: &euro;" + $(this).data('verkoopprijs')
										+ "</br>");
						$('.modal-show').append(
								"Status: " + $(this).data('status')
										+ "</br>");
						$('#show').openModal();
					});
			// END Modal show

			// START Modal edit
			$(document).on('click', '.edit', function() {
				
				function resetField(field) {
					$('#label_'  + field).removeAttr('data-error');
					$('.' + field).removeClass('invalid');
				}
				
				resetField('name');
				resetField('verkoopprijs');
				resetField('prijsperdag');
				resetField('borg');
				resetField('status');
				
				
				$('form.edit-form>.row>.input-field>.id').val($(this).data('id'));
				$('form.edit-form>.row>.input-field>.name').val($(this).data('name'));
				$('form.edit-form>.row>.input-field>.prijsperdag').val($(this).data('prijsperdag'));
				$('form.edit-form>.row>.input-field>.borg').val($(this).data('borg'));
				$('form.edit-form>.row>.input-field>.verkoopprijs').val($(this).data('verkoopprijs'));
				//$('form.edit-form>.row>.input-field>.status').val($(this).data('status'));
				

				
				
				var table = "edit";
				$('form.'+table+'-form>.row>.input-field>div>ul>li').removeClass('selected');
				
				if ($(this).data('status') == "Verhuurbaar") {
					console.log("Verhuurbaar");
					$('form.'+table+'-form>.row>.input-field>div>input').val('Verhuurbaar');
				}
				if ($(this).data('status') == "Verkoopbaar") {
					console.log("Verkoopbaar");
					$('form.'+table+'-form>.row>.input-field>div>input').val('Verkoopbaar');
				}
				if ($(this).data('status') == "Afgeschreven") {
					console.log("Afgeschreven");
					$('form.'+table+'-form>.row>.input-field>div>input').val('Afgeschreven');
				}
				 
				
				
				$('#edit').openModal();
				$('form.edit-form>.row>.input-field>label').addClass('active');
				$('form.edit-form>.row>.input-field>input').addClass('valid');
			});
			// END Modal edit

			// START Modal delete
			$(document).on('click', '.delete', function() {
				$('.data-delete').val($(this).data('id'));
				$('#delete').openModal();
			});
			// END Modal delete

			// Start Modal Yes Edit
			$(document).on(
					'click',
					'.modal-edit',
					function() {
						var update = true;
						
						
						if(checkField('name', 'edit', 40, 1) === false)
							update = false;
						if(checkField('prijsperdag', 'edit',  40, 1) === false)
							update = false;
						if(checkField('borg', 'edit',  40, 1) === false)
							update = false;
						if(checkField('verkoopprijs', 'edit',  40, 1) === false)
							update = false;
						
						if (update === true) {
							var url = "/api/boten/edit/";
							var data = "id=" + $('form.edit-form>.row>.input-field>.id').val();
							data += "&name=" + $('form.edit-form>.row>.input-field>.name').val();
							data += "&prijsperdag=" + $('form.edit-form>.row>.input-field>.prijsperdag').val();
							data += "&borg=" + $('form.edit-form>.row>.input-field>.borg').val();
							data += "&verkoopprijs=" + $('form.edit-form>.row>.input-field>.verkoopprijs').val();
							data += "&status=" + $('form.edit-form>.row>.input-field>div>input').val();
							$('.progress').css('visibility','visible');
							$.ajax({
								url : url,
								type : 'POST',
								data : data,
								success : function(result) {
									var obj = jQuery.parseJSON(result);
									console.log(obj[0]["succes"]);
									$('.progress').css('visibility','hidden');
									if (obj[0]["succes"] == true) {
										
										$('tr','#'+$('.id').val(),'td','name').append($('form.edit-form>.row>.input-field>.id').val());
										$('#'+$('.id').val()+'>#name').text($('form.edit-form>.row>.input-field>.name').val());
										$('#'+$('.id').val()+'>#email').text($('form.edit-form>.row>.input-field>.email').val());
										$('#'+$('.id').val()+'>#city').text($('form.edit-form>.row>.input-field>.city').val());
										
										function changeValue(field) {
											$('#'+$('.id').val()+'>.right>.green').attr('data-'+field ,$('form.edit-form>.row>.input-field>.'+field).val());
											// $('#'+$('.id').val()+'>.right>.yellow').attr('data-'+field
											// ,$('form.edit-form>.row>.input-field>.'+field).val());
										}
										changeValue('name');
										changeValue('prijsperdag');
										changeValue('borg');
										changeValue('verkoopprijs');
										changeValue('status');
										
										$('#'+$('.id').val()+'>#name').text($('.name').val());
										$('#'+$('.id').val()+'>#email').text($('.email').val());
										$('#'+$('.id').val()+'>#city').text($('.city').val());
										
										Materialize.toast(
												'Boot is gewijzigd', 4000,
												'rounded');
										$('.toast').css("backgroundColor",
												"rgb(0, 150, 0)");
										$('#edit').closeModal();
									} else {
										Materialize.toast(
												'Oeps! er is iets mis gegaan',
												4000, 'rounded');
										$('.toast').css("backgroundColor",
												"rgb(240, 0, 0)");
									}
								},
								  error: function(XMLHttpRequest, textStatus, errorThrown) {
									  $('.progress').css('visibility', 'hidden');
									  Materialize.toast('Oeps! er is iets mis gegaan',
												4000, 'rounded');
										$('.toast')
												.css("backgroundColor", "rgb(240, 0, 0)");
									  }
							});
						}
					});
			// END Modal Yes edit

			// Start Modal Yes delete
			$(document).on(
					'click',
					'.modal-delete',
					function() {
						var id = $('.data-delete').val();
						console.log("Pressed yes id: " + id);
						$('.progress').css('visibility','visible');
						var url = "/api/boten/delete/" + id;

						$.ajax({
							url : url,
							success : function(result) {
								var obj = jQuery.parseJSON(result);
								console.log(obj[0]["succes"]);
								$('.progress').css('visibility','hidden');
								if (obj[0]["succes"] == true) {
									$('.progress').css('visibility','hidden');
									Materialize.toast(
											'Boot is verwijderd', 4000,
											'rounded');
									$('.toast').css("backgroundColor",
											"rgb(0, 150, 0)");
									$('tr#'+id).remove();
									$('#delete').closeModal();
								} else {
									Materialize.toast(
											'Oeps! er is iets mis gegaan',
											4000, 'rounded');
									$('.toast').css("backgroundColor",
											"rgb(240, 0, 0)");
									$('#delete').closeModal();
								}
							},
							  error: function(XMLHttpRequest, textStatus, errorThrown) {
								  $('.progress').css('visibility', 'hidden');
								  Materialize.toast('Oeps! er is iets mis gegaan',
											4000, 'rounded');
									$('.toast')
											.css("backgroundColor", "rgb(240, 0, 0)");
								  }
						});
					});
			// END Modal Yes delete
			
			// START Modal add
			
			$(document).on(
					'click',
					'a.add',
					function() {
						console.log("Pressed Add");
						$('#add').openModal();
					});
			// END Modal add
			
			// START Modal Yes Add
			
			$(document).on(
					'click',
					'.modal-add',
					function() {
						var update = true;
						

						if(checkField('name', 'add', 40, 1) === false)
							update = false;
						console.log(update);
						if(checkField('prijsperdag', 'add',  40, 1) === false)
							update = false;
						console.log(update);
						if(checkField('borg', 'add',  40, 1) === false)
							update = false;
						console.log(update);
						if(checkField('verkoopprijs', 'add',  40, 1) === false)
							update = false;
						console.log(update);
						
						console.log(update);
						
						if (update === true) {
							var url = "/api/boten/add/";
							$('.progress').css('visibility','visible');
							var data = "name="+ $('form.add-form>.row>.input-field>.name').val();
							data += "&prijsperdag=" + $('form.add-form>.row>.input-field>.prijsperdag').val();
							data += "&borg=" + $('form.add-form>.row>.input-field>.borg').val();
							data += "&verkoopprijs=" + $('form.add-form>.row>.input-field>.verkoopprijs').val();
							data += "&status=" + $('form.add-form>.row>.input-field>div>input').val();

							$.ajax({
								url : url,
								type : 'POST',
								data : data,
								success : function(result) {
									var obj = jQuery.parseJSON(result);
									console.log(obj[0]["succes"]);
									$('.progress').css('visibility','hidden');
									if (obj[0]["succes"] == true) {
										
										Materialize.toast(
												'Boot is toegevoegd', 4000,
												'rounded');
										$('.toast').css("backgroundColor",
												"rgb(0, 150, 0)");
										$('#add').closeModal();
										$('form.add-form').trigger("reset");
										
										
										var dataTable = '<tr class="tr-row" id="'+obj[0]["id"]+'">'+
										'<td>'+obj[0]["id"]+'</td>'+
										'<td id="name">'+obj[0]["naam"]+'</td>'+
										'<td id="ppd" class="hide-on-med-and-down">&euro; '+obj[0]["prijsperdag"]+'</td>'+
										'<td id="ppd" class="hide-on-med-and-down">&euro; '+obj[0]["borg"]+'</td>'+

										'<td id="status">'+obj[0]["status"]+'</td>'+
										'<td class="right"><a'+
											' class="show btn-floating green hide-on-small-only"'+
											'data-id="'+obj[0]["id"]+'" data-name="'+obj[0]["naam"]+'"'+
											'data-prijsperdag="'+obj[0]["prijsperdag"]+'" data-borg="'+obj[0]["borg"]+'"'+
											'data-verkoopprijs="'+obj[0]["verkoopprijs"]+'"'+
											'+vdata-status="'+obj[0]["status"]+'"><i'+
												' class="material-icons" style="color: white;">visibility</i></a><a'+
											' class="edit btn-floating yellow hide-on-small-only"'+
											'data-id="'+obj[0]["id"]+'" data-name="'+obj[0]["naam"]+'"'+
											'data-prijsperdag="'+obj[0]["prijsperdag"]+'" data-borg="'+obj[0]["borg"]+'"'+
											'data-verkoopprijs="'+obj[0]["verkoopprijs"]+'"'+
											' data-status="'+obj[0]["status"]+'"><i class="material-icons"'+
												'style="color: white;">mode_edit</i></a><a'+
											' class="delete btn-floating red hide-on-small-only"'+
											'data-id="'+obj[0]["id"]+'" data-name="'+obj[0]["naam"]+'"'+
											'data-prijsperdag="'+obj[0]["prijsperdag"]+'" data-borg="'+obj[0]["borg"]+'"'+
											'data-verkoopprijs="'+obj[0]["verkoopprijs"]+'"'+
											'data-status="'+obj[0]["status"]+'"><i class="material-icons"'+
												'style="color: white;">delete</i></a>'+

											'<div '+
												' class="button-table right fixed-action-btn horizontal click-to-toggle hide-on-med-and-up">'+
												'<a class="btn-floating" style="background-color: #262B33;">'+
													'<i class="material-icons">menu</i>'+
												'</a>'+
												'<ul>'+
													'<li class="button-li"><a class="show btn-floating green"'+
														'data-id="'+obj[0]["id"]+'" data-name="'+obj[0]["naam"]+'"'+
														'data-prijsperdag="'+obj[0]["prijsperdag"]+'" data-borg="'+obj[0]["borg"]+'"'+
														'data-verkoopprijs="'+obj[0]["verkoopprijs"]+'"'+
														'+vdata-status="'+obj[0]["status"]+'"><i class="material-icons"'+
															'style="color: white;">visibility</i></a></li>'+
													'<li class="button-li"><a class="edit btn-floating yellow"'+
														'href="#edit"><i class="material-icons"'+
														'data-id="'+obj[0]["id"]+'" data-name="'+obj[0]["naam"]+'"'+
														'data-prijsperdag="'+obj[0]["prijsperdag"]+'" data-borg="'+obj[0]["borg"]+'"'+
														'data-verkoopprijs="'+obj[0]["verkoopprijs"]+'"'+
														'data-status="'+obj[0]["status"] +
															'style="color: white;">mode_edit</i></a></li>'+
													'<li class="button-li"><a class="delete btn-floating red"'+
														'href="#delete"><i class="material-icons"'+
														'data-id="'+obj[0]["id"]+'" data-name="'+obj[0]["naam"]+'"'+
														'data-prijsperdag="'+obj[0]["prijsperdag"]+'" data-borg="'+obj[0]["borg"]+'"'+
														'data-verkoopprijs="'+obj[0]["verkoopprijs"]+'"'+
														'data-status="'+obj[0]["status"]+ 
															'style="color: white;">delete</i></a></li>'+
												'</ul>'+
											'</div></td>'+
									'</tr>';
										
									$('.bordered.table_pagination>tbody').append(dataTable);
										
										
										
									} else {
										Materialize.toast(
												'Oeps! er is iets mis gegaan',
												4000, 'rounded');
										$('.toast').css("backgroundColor",
												"rgb(240, 0, 0)");
									}
								},
								  error: function(XMLHttpRequest, textStatus, errorThrown) {
									  $('.progress').css('visibility', 'hidden');
									  Materialize.toast('Oeps! er is iets mis gegaan',
												4000, 'rounded');
										$('.toast')
												.css("backgroundColor", "rgb(240, 0, 0)");
									  }
							});
						}
					});
			
			// END Modal Yes Add
		});