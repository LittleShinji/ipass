$(document).ready(
		
		function() {
			
			
			function checkEmpty(field, table, min) {
				
				if ($('form.'+table+'-form>.row>.input-field>.' + field).val().length < min) {
					$('form.'+table+'-form>.row>.input-field>>#label_'  + field).attr('data-error', 'Verplicht');
					$('form.'+table+'-form>.row>.input-field>.' + field).removeClass('valid');
					$('form.'+table+'-form>.row>.input-field>.' + field).addClass('invalid'); 
					return false;
				}
				return true;
			}
			
			function checkMax(field, table, max) {
				if ($('form.'+table+'-form>.row>.input-field>.' + field).val().length > max) {
					$('form.'+table+'-form>.row>.input-field>#label_'  + field).attr('data-error', 'Maximale karacters: ' + max);
					$('form.'+table+'-form>.row>.input-field>.' + field).removeClass('valid');
					$('form.'+table+'-form>.row>.input-field>.' + field).addClass('invalid'); 
					return false;
				}
				return true;
			}
			
			function checkField(field, table, max, min) {
				var update = true;
				if (checkEmpty(field, table, min) === false)
					update = false;
				if (checkMax(field, table, max) === false)
					update = false;
				return update;
			}
			
			// Start Modal Show
			$(document).on(
					'click',
					'.show',
					function() {
						$('.modal-show').empty();
						$('.modal-show')
								.append(
										"Klant nummer: " + $(this).data('id')
												+ "</br>");
						$('.modal-show').append(
								"Naam: " + $(this).data('name') + "</br>");
						$('.modal-show').append(
								"Email: " + $(this).data('email') + "</br>");
						$('.modal-show').append(
								"Adres: " + $(this).data('adres') + "</br>");
						$('.modal-show').append(
								"Woonplaats: " + $(this).data('city')
										+ "</br>");
						$('.modal-show').append(
								"Postcode: " + $(this).data('zipcode')
										+ "</br>");
						$('.modal-show').append(
								"Bankrekeningnummer: "
										+ $(this).data('iban')
										+ "</br>");
						$('#show').openModal();
					});
			// END Modal show

			// START Modal edit
			$(document).on('click', '.edit', function() {
				
				function resetField(field) {
					$('#label_'  + field).removeAttr('data-error');
					$('.' + field).removeClass('invalid');
				}
				
				resetField('name');
				resetField('email');
				resetField('adres');
				resetField('city');
				resetField('zipcode');
				resetField('iban');
				
				$('form.edit-form>.row>.input-field>.id').val($(this).data('id'));
				$('form.edit-form>.row>.input-field>.name').val($(this).data('name'));
				$('form.edit-form>.row>.input-field>.email').val($(this).data('email'));
				$('form.edit-form>.row>.input-field>.adres').val($(this).data('adres'));
				$('form.edit-form>.row>.input-field>.city').val($(this).data('city'));
				$('form.edit-form>.row>.input-field>.zipcode').val($(this).data('zipcode'));
				$('form.edit-form>.row>.input-field>.iban').val($(this).data('iban'));
				$('#edit').openModal();
			});
			// END Modal edit

			// START Modal delete
			$(document).on('click', '.delete', function() {
				$('.data-delete').val($(this).data('id'));
				$('#delete').openModal();
			});
			// END Modal delete

			// Start Modal Yes Edit
			$(document).on(
					'click',
					'.modal-edit',
					function() {
						var update = true;
						
						
						if(checkField('name', 'edit', 40, 5) === false)
							update = false;
						if(checkField('email', 'edit',  40, 5) === false)
							update = false;
						if(checkField('adres', 'edit',  40, 5) === false)
							update = false;
						if(checkField('city', 'edit',  40, 5) === false)
							update = false;
						if(checkField('zipcode', 'edit',  6, 5) === false)
							update = false;
						if(checkField('iban', 'edit',  18, 5) === false)
							update = false;
						
						if (update === true) {
							var url = "/api/klanten/edit/";
							var data = "id=" + $('form.edit-form>.row>.input-field>.id').val();
							data += "&name=" + $('form.edit-form>.row>.input-field>.name').val();
							data += "&email=" + $('form.edit-form>.row>.input-field>.email').val();
							data += "&adres=" + $('form.edit-form>.row>.input-field>.adres').val();
							data += "&city=" + $('form.edit-form>.row>.input-field>.city').val();
							data += "&zipcode=" + $('form.edit-form>.row>.input-field>.zipcode').val();
							data += "&iban=" + $('form.edit-form>.row>.input-field>.iban').val();
							$('.progress').css('visibility','visible');
							$.ajax({
								url : url,
								type : 'POST',
								data : data,
								success : function(result) {
									var obj = jQuery.parseJSON(result);
									console.log(obj[0]["succes"]);
									$('.progress').css('visibility','hidden');
									if (obj[0]["succes"] == true) {
										
										$('tr','#'+$('.id').val(),'td','name').append($('form.edit-form>.row>.input-field>.id').val());
										$('#'+$('.id').val()+'>#name').text($('form.edit-form>.row>.input-field>.name').val());
										$('#'+$('.id').val()+'>#email').text($('form.edit-form>.row>.input-field>.email').val());
										$('#'+$('.id').val()+'>#city').text($('form.edit-form>.row>.input-field>.city').val());
										
										function changeValue(field) {
											$('#'+$('.id').val()+'>.right>.green').attr('data-'+field ,$('form.edit-form>.row>.input-field>.'+field).val());
											//$('#'+$('.id').val()+'>.right>.yellow').attr('data-'+field ,$('form.edit-form>.row>.input-field>.'+field).val());
										}
										changeValue('name');
										changeValue('email');
										changeValue('city');
										changeValue('adres');
										changeValue('zipcode');
										changeValue('iban');
										
										$('#'+$('.id').val()+'>#name').text($('.name').val());
										$('#'+$('.id').val()+'>#email').text($('.email').val());
										$('#'+$('.id').val()+'>#city').text($('.city').val());
										
										Materialize.toast(
												'Gebruiker is gewijzigd', 4000,
												'rounded');
										$('.toast').css("backgroundColor",
												"rgb(0, 150, 0)");
										$('#edit').closeModal();
									} else {
										Materialize.toast(
												'Oeps! er is iets mis gegaan',
												4000, 'rounded');
										$('.toast').css("backgroundColor",
												"rgb(240, 0, 0)");
									}
								}
							});
						}
					});
			// END Modal Yes edit

			// Start Modal Yes delete
			$(document).on(
					'click',
					'.modal-delete',
					function() {
						var id = $('.data-delete').val();
						console.log("Pressed yes id: " + id);
						$('.progress').css('visibility','visible');
						var url = "/api/klanten/delete/" + id;

						$.ajax({
							url : url,
							success : function(result) {
								var obj = jQuery.parseJSON(result);
								console.log(obj[0]["succes"]);
								$('.progress').css('visibility','hidden');
								if (obj[0]["succes"] == true) {
									$('.progress').css('visibility','hidden');
									Materialize.toast(
											'Gebruiker is verwijderd', 4000,
											'rounded');
									$('.toast').css("backgroundColor",
											"rgb(0, 150, 0)");
									$('tr#'+id).remove();
									$('#delete').closeModal();
								} else {
									Materialize.toast(
											'Oeps! er is iets mis gegaan',
											4000, 'rounded');
									$('.toast').css("backgroundColor",
											"rgb(240, 0, 0)");
									$('#delete').closeModal();
								}
							}
						});
					});
			// END Modal Yes delete
			
			//START Modal add
			
			$(document).on(
					'click',
					'a.add',
					function() {
						console.log("Pressed Add");
						$('#add').openModal();
					});
			//END Modal add
			
			//START Modal Yes Add
			
			$(document).on(
					'click',
					'.modal-add',
					function() {
						var update = true;
						
						
						if(checkField('name', 'add', 40, 5) === false)
							update = false;
						if(checkField('email', 'add',  40, 1) === false)
							update = false;
						if(checkField('adres', 'add',  40, 1) === false)
							update = false;
						if(checkField('city', 'add',  40, 1) === false)
							update = false;
						if(checkField('zipcode', 'add',  6, 5) === false)
							update = false;
						if(checkField('iban', 'add',  18, 1) === false)
							update = false;
						
						if (update === true) {
							var url = "/api/klanten/add/";
							$('.progress').css('visibility','visible');
							var data = "name=" + $('form.add-form>.row>.input-field>.name').val();
							data += "&email=" + $('form.add-form>.row>.input-field>.email').val();
							data += "&adres=" + $('form.add-form>.row>.input-field>.adres').val();
							data += "&city=" + $('form.add-form>.row>.input-field>.city').val();
							data += "&zipcode=" + $('form.add-form>.row>.input-field>.zipcode').val();
							data += "&iban=" + $('form.add-form>.row>.input-field>.iban').val();

							$.ajax({
								url : url,
								type : 'POST',
								data : data,
								success : function(result) {
									var obj = jQuery.parseJSON(result);
									console.log(obj[0]["succes"]);
									$('.progress').css('visibility','hidden');
									if (obj[0]["succes"] == true) {
										
										Materialize.toast(
												'Gebruiker is toegevoegd', 4000,
												'rounded');
										$('.toast').css("backgroundColor",
												"rgb(0, 150, 0)");
										$('#add').closeModal();
										$('form.add-form').trigger("reset");
										 location.reload();
									} else {
										Materialize.toast(
												'Oeps! er is iets mis gegaan',
												4000, 'rounded');
										$('.toast').css("backgroundColor",
												"rgb(240, 0, 0)");
									}
								}
							});
						}
					});
			
			//END Modal Yes Add
		});