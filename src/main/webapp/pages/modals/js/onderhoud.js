
window.currentPage = null;
var lastPage = null;

var SelectedStartDate = null;
var SelectedEindDate = null;
var date = new Date();
var selectedBoot = null;
var data = [];

window.currentPage = 1;
lastPage = 1;
initTabs(window.currentPage);

// Modal Volgende
$(document).on('click', '.modal-footer>.next.active', function() {
	window.currentPage = window.currentPage + 1;
	if (lastPage <= window.currentPage)
		lastPage = window.currentPage;
	//console.log(window.currentPage);
	initTabs(window.currentPage, lastPage);

	//console.log(lastPage + " " + window.currentPage);

	if (lastPage == window.currentPage) {
		$('.modal-footer>.next').addClass('disabled');
		$('.modal-footer>.next').removeClass('active');
	}
	$('.modal-footer>.previous').removeClass('disabled');
	$('.modal-footer>.previous').addClass('active');
});

// Modal vorige
$(document).on('click', '.modal-footer>.previous.active', function() {
	window.currentPage = currentPage - 1;
	//console.log(window.currentPage);
	initTabs(window.currentPage, lastPage);

	if (window.currentPage <= 1) {
		$('.modal-footer>.previous').addClass('disabled');
		$('.modal-footer>.previous').removeClass('active');
	}

	if (lastPage >= window.currentPage) {
		$('.modal-footer>.next').removeClass('disabled');
		$('.modal-footer>.next').addClass('active');
	}
});

// Modal tab header
$(document).on('click', '.modal_tabs>.click', function() {
	//console.log('Clicked on : ' + $(this).data('page'));
	initTabs($(this).data('page'), lastPage);
	window.currentPage = $(this).data('page');

});

// Modal tabs aanmaken
function initTabs(page, lastday) {
	$('.tab_modal').hide();
	$('.modal_tabs>li').removeClass('active');
	$('.button-boeking').removeClass('next');
	$('.button-boeking').removeClass('save');
	$('.button-boeking').text("Volgende");

	window.currentPage = page;
	var lastDay = lastday;

	if (page == 1) {
		//console.log('Change page to 1');
		$('.chooseboat').show();
		$('.modal_tabs>.chooseboat').addClass('active');
		$('.modal_tabs>.chooseboat').addClass('click');
		$('.button-boeking').addClass('next');

		$('.modal-footer>.previous').addClass('disabled');
		$('.modal-footer>.previous').removeClass('active');

		if (lastPage == window.currentPage) {
			$('.modal-footer>.next').addClass('disabled');
			$('.modal-footer>.next').removeClass('active');
		}

		if (lastPage > window.currentPage) {
			$('.modal-footer>.next').removeClass('disabled');
			$('.modal-footer>.next').addClass('active');
		}
	}
	if (page == 2) {
		//console.log('Change page to 3');
		$('.choosedate').show();
		$('.modal_tabs>.choosedate').addClass('active');
		$('.modal_tabs>.choosedate').addClass('click');
		$('.button-boeking').addClass('next');

		$('.modal-footer>.previous').removeClass('disabled');
		$('.modal-footer>.previous').addClass('active');

		if (lastPage > window.currentPage) {
			$('.modal-footer>.next').removeClass('disabled');
			$('.modal-footer>.next').addClass('active');
		}
		
		initAgenda(date);
	}
	if (page == 3) {
		//console.log('Change page to 4');
		$('.confirmation').show();
		$('.modal_tabs>.confirmation').addClass('active');
		$('.modal_tabs>.confirmation').addClass('click');

		$('.modal-footer>.previous').removeClass('disabled');
		$('.modal-footer>.previous').addClass('active');

		$('.button-boeking').addClass('save');
		$('.button-boeking').text("Opslaan");

	}
}

function checkEmpty(field, table, min) {
	if ($('table.'+table+'>tbody>tr>.' + field).text().length == min) {
		return false;
	}
	return true;
}

function checkMax(field, table, max) {
	if ($('table.'+table+'>tbody>tr>.' + field).text().length < max) {
		return false;
	}
	return true;
}

function checkField(field, table, min, max) {
	var update = true;
	if (checkEmpty(field, table, min) === false)
		update = false;
	console.log(update);
	if (checkMax(field, table, max) === false)
		update = false;
	console.log(update);
	return update;
}

// Start Modal Show
$(document).on(
		'click',
		'.show',
		function() {
			
			$('.show-boot>tbody>tr>.id').text($(this).data('boot-id'));
			$('.show-boot>tbody>tr>.name').text($(this).data('boot-naam'));
			$('.show-boot>tbody>tr>.prijsperdag').html('&euro; ' + $(this).data('boot-prijs_per_dag'));
			$('.show-boot>tbody>tr>.borg').html('&euro; ' + $(this).data('boot-borg'));
			
			$('.show-boeking>tbody>tr>.startdate').text($(this).data('startdatum'));
			$('.show-boeking>tbody>tr>.enddate').text($(this).data('einddatum'));
			
			$('#show').openModal();
		});
// END Modal show


// START Modal delete
$(document).on('click', '.delete', function() {
	$('.data-delete').val($(this).data('id'));
	$('#delete').openModal();
});
// END Modal delete


// Start Modal Yes delete
$(document).on(
		'click',
		'.modal-delete',
		function() {
			var id = $('.data-delete').val();
			//console.log("Pressed yes id: " + id);
			$('.progress').css('visibility', 'visible');
			var url = "/api/onderhoud/delete/" + id;

			$.ajax({
				url : url,
				success : function(result) {
					var obj = jQuery.parseJSON(result);
					//console.log(obj[0]["succes"]);
					$('.progress').css('visibility', 'hidden');
					if (obj[0]["succes"] == true) {
						$('.progress').css('visibility', 'hidden');
						Materialize
								.toast('Boeking is verwijderd', 4000, 'rounded');
						$('.toast').css("backgroundColor", "rgb(0, 150, 0)");
						$('.table_pagination>tbody>tr.' + id).remove();
						$('#delete').closeModal();
					} else {
						Materialize.toast('Oeps! er is iets mis gegaan', 4000,
								'rounded');
						$('.toast').css("backgroundColor", "rgb(240, 0, 0)");
						$('#delete').closeModal();
					}
				},
				  error: function(XMLHttpRequest, textStatus, errorThrown) {
					  $('.progress').css('visibility', 'hidden');
					  Materialize.toast('Oeps! er is iets mis gegaan',
								4000, 'rounded');
						$('.toast')
								.css("backgroundColor", "rgb(240, 0, 0)");
					  }
			});
		});
// END Modal Yes delete

// START Modal add

$(document).on('click', 'a.add', function() {
	//console.log("Pressed Add");

	window.currentPage = 1;
	var lastPage = 1;

	$('.modal_tabs>li').removeClass('click');
	$('table.boot>tbody>tr').css('backgroundColor', 'transparent');
	
	initTabs(window.currentPage, lastPage);

	//console.log('Reset');
	window.currentPage = 1;
	lastPage = 1;

	init_pagination('.boot', '.boot_pagination');

	SelectedStartDate = null;
	SelectedEindDate = null;
	initAgenda(date);
	selectedBoot = null;
	
	$('#add').openModal();
});
// END Modal add

// START Modal Yes Add

$(document).on(
		'click',
		'.save',
		function() {
			var update = true;

			if (checkField('id', 'data-boot', 10, 0) === false)
				update = false;
			if (checkField('startdate', 'data-boeking', 10, 0) === false)
				update = false;
			if (checkField('startdate', 'data-boeking', 10, 0) === false)
				update = false;
			
			console.log(update);
			
			if (update === true) {
				
				var url = "/api/onderhoud/add/";
				$('.progress').css('visibility', 'visible');
				
				var data = "id-boot="
						+ $('table.data-boot>tbody>tr>.id').text();
				data += "&start-date="
						+ $('table.data-boeking>tbody>tr>.startdate').text();
				data += "&end-date="
						+ $('table.data-boeking>tbody>tr>.startdate').text();

				$.ajax({
					url : url,
					type : 'POST',
					data : data,
					success : function(result) {
						var obj = jQuery.parseJSON(result);
						//console.log(obj[0]["succes"]);
						$('.progress').css('visibility', 'hidden');
						if (obj[0]["succes"] == true) {

							Materialize.toast('Boeking is toegevoegd', 4000,
									'rounded');
							$('.toast')
									.css("backgroundColor", "rgb(0, 150, 0)");
							$('#add').closeModal();
							$('form.add-form').trigger("reset");
							
							// Add toe table
							 location.reload();
							
						} else {
							Materialize.toast('Oeps! er is iets mis gegaan',
									4000, 'rounded');
							$('.toast')
									.css("backgroundColor", "rgb(240, 0, 0)");
						}
					},
					  error: function(XMLHttpRequest, textStatus, errorThrown) {
						  $('.progress').css('visibility', 'hidden');
						  Materialize.toast('Oeps! er is iets mis gegaan',
									4000, 'rounded');
							$('.toast')
									.css("backgroundColor", "rgb(240, 0, 0)");
						  }
				});
			}
		});

// END Modal Yes Add

// START AGENDA

//console.log("works?");


$(document).on(
		'click',
		'.boot>tbody>tr',
		function() {
			$('.boot>tbody>tr').css('backgroundColor', 'transparent');
			$(this).css("backgroundColor", 'gray');
			//console.log('Clicked: ' + $(this).data('id'));
			$('table.data-boot>tbody>tr>.id').html($(this).data('id'));
			selectedBoot = $(this).data('id');
			$('table.data-boot>tbody>tr>.name').html($(this).data('naam'));
			$('table.data-boot>tbody>tr>.prijsperdag').html(
					'&euro; ' + $(this).data('prijsperdag'));
			$('table.data-boot>tbody>tr>.borg').html(
					'&euro; ' + $(this).data('borg'));
			$('.modal-footer>.next').addClass(
					'"waves-effect waves-green active');
			$('.modal-footer>.next').removeClass('disabled');
			SelectedStartDate = null;
			SelectedEindDate = null;
			lastPage = 3;
			
		});

$(document).on(
		'click',
		'.agenda>.days>.selected',
		function() {
			if (SelectedStartDate != null && SelectedEindDate == null) {
				//console.log(SelectedStartDate == SelectedEindDate);
				SelectedEindDate = new Date($(this).data('year'), $(this)
						.data('month'), $(this).data('day'));
				
				$('table.data-boeking>tbody>tr>.enddate').html(
						SelectedEindDate.getDate() + "-"
								+ SelectedEindDate.getMonth() + "-"
								+ SelectedEindDate.getFullYear());
				//console.log(SelectedEindDate +' '+ SelectedStartDate);
				var days_between = Math
						.round((SelectedEindDate - SelectedStartDate)
								/ (1000 * 60 * 60 * 24)) + 1;
				var price = parseFloat($(
						'table.data-boot>tbody>tr>.prijsperdag').html()
						.slice(2));
				var borg = parseFloat($(
						'table.data-boot>tbody>tr>.borg').html().slice(
						2));
				$('table.data-boeking>tbody>tr>.price ').html(
						'&euro;' + price * days_between);
				$('table.data-boeking>tbody>tr>.borg ').html(
						'&euro;' + borg);
				$('table.data-boeking>tbody>tr>.total ').html(
						'&euro;' + ((price * days_between) + borg));
				$('.modal-footer>.next').addClass('active');
				$('.modal-footer>.next').removeClass('disabled');
			}
		});

$(document).on(
		'click',
		'.agenda>.days>.enabled',
		function() {
			if (SelectedStartDate != null && SelectedEindDate != null) {
				//console.log('test');
				SelectedStartDate = null;
				SelectedEindDate = null;
				initAgenda(date);
				//console.log(SelectedStartDate);
				$('.modal-footer>.next').addClass('disabled');
				$('.modal-footer>.next').removeClass('active');
				return;
			}
			if (SelectedStartDate == null) {
				SelectedStartDate = new Date($(this).data('year'), $(this)
						.data('month'), $(this).data('day'));
				initAgenda(date);
				//console.log(SelectedStartDate);
				$('table.data-boeking>tbody>tr>.startdate').html(
						SelectedStartDate.getDate() + "-"
								+ SelectedStartDate.getMonth() + "-"
								+ SelectedStartDate.getFullYear());
			} else {
				if (SelectedEindDate == null) {
					SelectedEindDate = new Date($(this).data('year'), $(this)
							.data('month'), $(this).data('day'));
					
					var boolean = true;
					
					data.forEach(function(item) {
						
						if (SelectedStartDate <= item[0] && SelectedEindDate >= item[1]) {
							boolean = false;
						}
					});
					
					if (SelectedEindDate >= SelectedStartDate && boolean == true) {
						initAgenda(date);
						//console.log(SelectedEindDate);
						$('table.data-boeking>tbody>tr>.enddate').html(
								SelectedEindDate.getDate() + "-"
										+ SelectedEindDate.getMonth() + "-"
										+ SelectedEindDate.getFullYear());

						var days_between = Math
								.round((SelectedEindDate - SelectedStartDate)
										/ (1000 * 60 * 60 * 24)) + 1;
						var price = parseFloat($(
								'table.data-boot>tbody>tr>.prijsperdag').html()
								.slice(2));
						var borg = parseFloat($(
								'table.data-boot>tbody>tr>.borg').html().slice(
								2));
						$('table.data-boeking>tbody>tr>.price ').html(
								'&euro;' + price * days_between);
						$('table.data-boeking>tbody>tr>.borg ').html(
								'&euro;' + borg);
						$('table.data-boeking>tbody>tr>.total ').html(
								'&euro;' + ((price * days_between) + borg));
						$('.modal-footer>.next').addClass('active');
						$('.modal-footer>.next').removeClass('disabled');

					} else {
						SelectedEindDate = null;
						SelectedStartDate = new Date($(this).data('year'), $(this)
								.data('month'), $(this).data('day'));
						initAgenda(date);
						//console.log(SelectedStartDate);
						$('table.data-boeking>tbody>tr>.startdate').html(
								SelectedStartDate.getDate() + "-"
										+ SelectedStartDate.getMonth() + "-"
										+ SelectedStartDate.getFullYear());
					}
				}
			}
		});

var months = [ "Januari", "Febauri", "Maart", "April", "Mei", "Juni", "Juli",
		"Augustus", "September", "Oktober", "November", "December" ];

initAgenda(date);

$(document).on('click', '.agenda>.header>div>.links', function() {
	//console.log("Maand terug");
	date.setMonth(date.getMonth() - 1);
	initAgenda(date);
});

$(document).on('click', '.agenda>.header>div>.rechts', function() {
	//console.log("Maand verder");
	date.setMonth(date.getMonth() + 1);
	initAgenda(date);
});

function initAgenda(date) {
	
	$('.agenda>.days').html('');
	var month = date.getMonth();
	var year = date.getFullYear();

	var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

	var selectedMonthName = months[date.getMonth()];
	$('.agenda>.header>.month').text(selectedMonthName + " " + year);

	data = [];
	
	if (lastPage == window.currentPage) {
		$('.modal-footer>.next').addClass('disabled');
		$('.modal-footer>.next').removeClass('active');
	}
	
	if (selectedBoot != null) {
	$('.bordered.table_pagination>tbody>tr').each(function(index, value) {
		console.log(selectedBoot);
		if ($(this).data('id') == selectedBoot) {
			var startdate = $(this).find('#startdate').text().split('-');
			var enddate = $(this).find('#enddate').text().split('-');
			var datestart = new Date(startdate[0] ,startdate[1] - 1,startdate[2]);
			var dateend = new Date(enddate[0] ,enddate[1] - 1 ,enddate[2]);
			var adddata = [datestart, dateend];
			data.push(adddata);
		}
		
	});
	$('.bordered.data-boeking>tbody>tr').each(function(index, value) {
		console.log(selectedBoot);
		if ($(this).data('id') == selectedBoot) {
			var startdate = $(this).find('#startdate').text().split('-');
			var enddate = $(this).find('#enddate').text().split('-');
			var datestart = new Date(startdate[0] ,startdate[1] - 1,startdate[2]);
			var dateend = new Date(enddate[0] ,enddate[1] - 1 ,enddate[2]);
			var adddata = [datestart, dateend];
			data.push(adddata);
		}
		
	});
	}
	date = date.setDate(1);
	for (var i = 1; i <= lastDay.getDate(); i++) {
		var agendaDate = new Date(year, month, i);

		checkDay(agendaDate);
	}

	function checkDay(date) {
		var checkDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
		
		var today = new Date();
		today = new Date(today.getFullYear(), today.getMonth(), today.getDate());

		var yesterday = new Date(today.getFullYear(), today.getMonth(), today
				.getDate() - 1);
		
		
		for (var i = 0; i < data.length; i++) {
			var set = false;
			
			if(data[i][0] <= date && data[i][1] >= date) {
				addDisabled(date.getDate(), date.getMonth(), date.getFullYear());
				return;
			}
		}
		
		if (yesterday >= date) {
			addDisabled(date.getDate(), date.getMonth(), date.getFullYear());
			return;
		}
		

		if (SelectedStartDate != null && SelectedEindDate == null) {
			if (SelectedStartDate.getFullYear() == date.getFullYear()
					&& SelectedStartDate.getMonth() == date.getMonth()
					&& SelectedStartDate.getDate() == date.getDate()) {
				addSelected(date.getDate(), date.getMonth(), date.getFullYear());
				return;
			}
		}

		if (SelectedStartDate != null && SelectedEindDate != null) {
			if (SelectedEindDate >= date && SelectedStartDate <= date) {
				addSelected(date.getDate(), date.getMonth(), date.getFullYear());
				return;
			}
		}

		addEnabled(date.getDate(), date.getMonth(), date.getFullYear());
	}

	function addDisabled(dayNumber, monthNumber, yearNumber) {
		var today = new Date();
		if (dayNumber == today.getDate() && monthNumber == today.getMonth()
				&& yearNumber == today.getFullYear()) {
			$('.agenda>.days').append(
					"<div class=\"day z-depth-1 today disabled\"><p>"
							+ dayNumber + "</p></div>");
		} else {
			$('.agenda>.days').append(
					"<div class=\"day z-depth-1 disabled\"><p>" + dayNumber
							+ "</p></div>");
		}
	}
	function addEnabled(dayNumber, monthNumber, yearNumber) {
		var today = new Date();
		if (dayNumber == today.getDate() && monthNumber == today.getMonth()
				&& yearNumber == today.getFullYear()) {
			$('.agenda>.days').append(
					"<div data-day=\"" + dayNumber + "\" data-month=\""
							+ monthNumber + "\" data-year=\"" + yearNumber
							+ "\" class=\"day z-depth-1 today enabled\"><p>"
							+ dayNumber + "</p></div>");
		} else {
			$('.agenda>.days').append(
					"<div data-day=\"" + dayNumber + "\" data-month=\""
							+ monthNumber + "\" data-year=\"" + yearNumber
							+ "\" class=\"day z-depth-1 enabled\"><p>"
							+ dayNumber + "</p></div>");
		}
	}
	function addSelected(dayNumber, monthNumber, yearNumber) {
		var today = new Date();
		if (dayNumber == today.getDate() && monthNumber == today.getMonth()
				&& yearNumber == today.getFullYear()) {
			$('.agenda>.days').append(
					"<div data-day=\"" + dayNumber + "\" data-month=\""
							+ monthNumber + "\" data-year=\"" + yearNumber
							+ "\" class=\"day z-depth-1 selected today\"><p>"
							+ dayNumber + "</p></div>");
		} else {
			$('.agenda>.days').append(
					"<div data-day=\"" + dayNumber + "\" data-month-\""
							+ monthNumber + "\" data-year=\"" + yearNumber
							+ "\" class=\"day z-depth-1 selected\"><p>"
							+ dayNumber + "</p></div>");
		}

	}
}

// END Agenda

$(document).ready(
function() {
	
	
});