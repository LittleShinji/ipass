<div id="show" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Klant gegevens</h4>
		<div class="modal-show"></div>
	</div>
	<div class="modal-footer">
		<a class="modal-action modal-close waves-effect waves-green btn-flat ">Sluit</a>
	</div>
</div>

<div id="edit" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Klant gegevens</h4>
		<div class="row">
			<form class="col s12 edit-form">
				<div class="row">
					<div class="input-field col s12">
						<input disabled type="text" class="validate id" value="&nbsp;"> <label
							for="disabled">Klanten nummer</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="name" type="text" class="validate name" value="&nbsp;"> <label
							id="label_name" for="name">Naam</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="email" type="email" class="validate email" value="&nbsp;"> <label
							id="label_email" for="email">Email</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="adres" type="text" class="validate adres" value="&nbsp;"> <label
							id="label_adres" for="adres">Adres</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="city" type="text" class="validate city" value="&nbsp;"> <label
							id="label_city" for="city">Woonplaats</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="zipcode" type="text" class="validate zipcode" value="&nbsp;"> <label
							id="label_zipcode" for="zipcode">Postcode</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="iban" type="text" class="validate iban" value="&nbsp;"> <label
							id="label_iban" for="iban">Bankrekening nummer</label>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="modal-footer">
		<a class="modal-action modal-close waves-effect waves-green btn-flat ">Annuleren</a>
		<a class="modal-edit waves-effect waves-green btn-flat ">Opslaan</a>
	</div>
</div>

<div id="delete" class="div-delete modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Verwijderen</h4>
		<p>Weet u het zeker ?</p>
	</div>
	<div class="modal-footer">
		<input class="data-delete" type="hidden" value="&nbsp;">
		<a class="modal-action modal-close waves-effect waves-green btn-flat ">Nee</a>
		<a class="modal-delete waves-effect waves-green btn-flat">Ja</a>
	</div>
</div>

<div id="add" class="modal modal-fixed-footer">
	<div class="modal-content">
		<h4>Klant gegevens</h4>
		<div class="row">
			<form class="col s12 add-form">
				<div class="row">
					<div class="input-field col s12">
						<input disabled type="text" class="validate id" value="&nbsp;"> <label
							for="disabled">Klanten nummer</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="name" type="text" class="validate name" value="&nbsp;"> <label
							id="label_name" for="name">Naam</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="email" type="email" class="validate email" value="&nbsp;"> <label
							id="label_email" for="email">Email</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="adres" type="text" class="validate adres" value="&nbsp;"> <label
							id="label_adres" for="adres">Adres</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="city" type="text" class="validate city" value="&nbsp;"> <label
							id="label_city" for="city">Woonplaats</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="zipcode" type="text" class="validate zipcode" value="&nbsp;"> <label
							id="label_zipcode" for="zipcode">Postcode</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="iban" type="text" class="validate iban" value="&nbsp;"> <label
							id="label_iban" for="iban">Bankrekening nummer</label>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="modal-footer">
		<a class="modal-action modal-close waves-effect waves-green btn-flat ">Annuleren</a>
		<a class="modal-add waves-effect waves-green btn-flat">Opslaan</a>
	</div>
</div>

<script src="/pages/modals/js/klant.js"></script>