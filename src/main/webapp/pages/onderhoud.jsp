<%@ page import="com.IPASS.model.*"%>
<%@ page import="com.IPASS.Service.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="col s12">
	<div class="panel">

<table class="bordered data-boeking" style="display: none">
			<tbody>

				<c:forEach var="item"
					items="${ServiceProvider.getBoekingService().getAll()}">
					<tr class="tr-row ${item.id}" data-id="${item.boot.id}">
						<td id="startdate" class="">${item.getStartdatum()}</td>
						<td id="enddate" class="">${item.getEinddatum()}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>


		<table class="bordered table_pagination">
			<thead>
				<tr>
					<td>ID</td>
					<td class="hide-on-med-and-down">Boot naam</td>
					<td>Begin datum</td>
					<td class="hide-on-med-and-down">Eind datum</td>
					<td></td>
				</tr>
			</thead>
			<tbody>

				<c:forEach var="item"
					items="${ServiceProvider.getOnderhoudService().getAll()}">
					<tr class="tr-row ${item.id}" data-id="${item.boot.id}">
						<td>${item.id}</td>
						<td id="bname" class="hide-on-med-and-down">
							${item.getBoot().getNaam()}</td>
						<td id="startdate" class="hide-on-med-and-down">${item.getStartdatum()}</td>
						<td id="enddate" class="hide-on-med-and-down">${item.getEinddatum()}</td>

						<td class="right"><a
							class="show btn-floating green hide-on-small-only"
							data-id="${item.id}" data-startdatum="${item.startdatum}"
							data-einddatum="${item.einddatum}"
							data-boot-id="${item.boot.id}" data-boot-borg="${item.boot.borg}"
							data-boot-naam="${item.boot.naam}"
							data-boot-prijs_per_dag="${item.boot.prijs_per_dag}"><i
								class="material-icons" style="color: white;">visibility</i></a><a
							class="delete btn-floating red hide-on-small-only"
							data-id="${item.id}"><i class="material-icons"
								style="color: white;">delete</i></a>

							<div
								class="button-table right fixed-action-btn horizontal click-to-toggle hide-on-med-and-up">
								<a class="btn-floating" style="background-color: #262B33;">
									<i class="material-icons">menu</i>
								</a>
								<ul>
									<li class="button-li"><a class="show btn-floating green"
										data-id="${item.id}" data-startdatum="${item.startdatum}"
										data-einddatum="${item.einddatum}"
										data-boot-id="${item.boot.id}"
										data-boot-borg="${item.boot.borg}"
										data-boot-verkoop_prijs="${item.boot.verkoop_prijs}"
										data-boot-prijs_per_dag="${item.boot.prijs_per_dag}"
										data-boot-status="${item.boot.status}"><i
											class="material-icons" style="color: white;">visibility</i></a></li>

									<li class="button-li"><a class="delete btn-floating red"
										href="#delete"><i class="material-icons"
											style="color: white;">delete</i></a></li>
								</ul>
							</div></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<ul class="pagination table_pagina center">
		</ul>
	</div>
</div>
<div class="fixed-action-btn" style="bottom: 45px; right: auto;">
	<a class="add btn-floating btn-large black"> <i
		class="large material-icons">add</i>
	</a>
</div>

<%@include file="/pages/modals/onderhoud.jsp"%>