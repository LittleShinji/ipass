<html>
<head>
<title>Oeps! pagina niet gevonden</title>
<link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link type="text/css" rel="stylesheet"
	href="../materialize/css/materialize.min.css" media="screen,projection" />
<!--Let browser know website is optimized for mobile-->
<link type="text/css" rel="stylesheet" href="../css/404.css" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body class="valign-wrapper center">

<div class="err-container text-center ">
        <div>
            <h1>404</h1>
            <h2>Oeps! Pagina niet gevonden</h2>
        </div>

        <div class="button404">
            <a href="/admin/" class="button404">
                Ga terug
            </a>
        </div>
    </div>

	<script type="text/javascript"
		src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="../materialize/js/materialize.min.js"></script>
	
</body>
</html>
