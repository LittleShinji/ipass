
<div class="navbar-fixed white">
	<nav>
		<div class="nav-wrapper white">
		<a href="#" data-activates="slide-out" class="button-collapse"><i
	class="mdi-navigation-menu material-icons">view_headline</i></a>
			<a href="/admin/" class="brand-logo"><img class="header-logo" src="/images/Pietsbotenverhuurverticaal.png" /></a>
			<ul id="nav-mobile" class="right hide-on-med-and-down">
				<li class="valign"><a href="#" style="height: 100%;" class="valign-wrapper"><img class="profilphoto" src="/images/${LoggedUser.naam}.jpg"/><p class="profilname">${LoggedUser.naam}</p></a></li>
				<li><a href="/logout"><i class="material-icons small">power_settings_new</i></a></li>
			</ul>
		</div>
	</nav>
</div>