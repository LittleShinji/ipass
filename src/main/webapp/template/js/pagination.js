$(document).ready(

function() {

	init_pagination('.table_pagination', '.table_pagina');
});

$(document).on('click', '.save', function() {
	console.log('click func');
	init_pagination('.table_pagination', '.table_pagina');
});

function init_pagination(table, pagination) {
	console.log('init_pagina');
	var rowCount = $(table + '>tbody>tr').length;
	var pages = Math.ceil(rowCount / 20);
	var currentPage = 1;
	console.log("RowCount: " + rowCount + "\nPages: " + pages);
	$(pagination + '').html('');
	if (pages != 1) {
		$(pagination + '')
				.append(
						"<li class=\"first\"><a><i class=\"material-icons\">chevron_left</i></a></li>");
		for (var i = 1; i <= pages; i++) {
			$(pagination + '').append(
					"<li class=\"waves-effect\" data-page=\"" + i + "\"><a>"
							+ i + "</a></li>");
			console.log(i);
		}
		$(pagination + '')
				.append(
						"<li class=\"last\"><a><i class=\"material-icons\">chevron_right</i></a></li>");

	}
	showTablePage(1);

	function showTablePage(pageID) {
		$(table + '>tbody>tr').addClass('hide');

		for (var i = (pageID - 1) * 20; i < (pageID * 20) + 1; i++) {
			$(table + '>tbody>tr:lt(' + i + ')').removeClass('hide');

		}
		for (var i = 0; i < ((pageID - 1) * 20) + 1; i++) {
			$(table + '>tbody>tr:lt(' + i + ')').addClass('hide');
		}
		changeActiveButton(pageID)
		i = 0;
	}

	function changeActiveButton(pageID) {
		$(pagination + '>.waves-effect').removeClass('active');
		$(pagination + '>.waves-effect:lt(' + pageID + ')').addClass('active');
		$(pagination + '>.waves-effect:lt(' + (pageID - 1) + ')').removeClass(
				'active');
	}
	;

	$(document).on('click', pagination + '>.waves-effect', function() {
		showTablePage($(this).data("page"));
		currentPage = $(this).data("page");
	});

	$(document).on('click', pagination + '>.first', function() {
		if ((currentPage - 1) >= 1) {
			currentPage -= 1;
			showTablePage(currentPage);
			$(window).scrollTop();
		}
	});
	$(document).on('click', pagination + '>.last', function() {
		if (currentPage + 1 <= pages) {
			currentPage += 1;
			showTablePage(currentPage);
			$(window).scrollTop();
		}

	});
}
