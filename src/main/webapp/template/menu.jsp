<ul id="slide-out" class="side-nav fixed">
	<li class="nav-header">Navigatie</li>
	<li class="menu-item valign"><a href="/admin/"
		class="valign-wrapper waves-effect"><i
			class="small material-icons">store</i>Dashboard</a></li>

	<li class="menu-item valign"><a
		class="valign-wrapper waves-effect" href="/admin/boekingen"><i
			class="small material-icons">event</i>Boekingen</a></li>

	<li class="menu-item valign"><a
		class="valign-wrapper waves-effect" href="/admin/boten"><i
			class="small material-icons">directions_boat</i>Boten</a></li>

	<li class="menu-item valign"><a
		class="valign-wrapper waves-effect" href="/admin/onderhoud"><i
			class="small material-icons" >build</i>Onderhoud</a></li>
	<li class="menu-item valign"><a href="/admin/klanten"
		class="valign-wrapper waves-effect"><i
			class="small material-icons">perm_identity</i>Klanten</a></li>
	<!-- <li class="menu-item valign"><a href="/admin/facturen"
		class="valign-wrapper waves-effect"><i
			class="small material-icons">trending_up</i>Facturen</a></li>-->
</ul>

<script>
	$(document).ready(function() {
		$('.button-collapse').sideNav({
			menuWidth : 240, // Default is 240
			edge : 'left', // Choose the horizontal origin
			closeOnClick : false
		// Closes side-nav on <a> clicks, useful for Angular/Meteor
		});
	});
	// Hide sideNav
</script>
