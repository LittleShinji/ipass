<html>
<head>
<title>Piet's botenverhuur</title>
<link href="http://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link type="text/css" rel="stylesheet"
	href="/materialize/css/materialize.min.css" media="screen,projection" />
<!--Let browser know website is optimized for mobile-->
<link type="text/css" rel="stylesheet" href="/css/custom.css" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript"
		src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
</head>
<body>
<div class="progress black">
      <div class="indeterminate"></div>
  </div>
<header>
<%@include file="head.jsp" %>
</header>
<main>
<%@include file="menu.jsp" %>

<div class="main-container"><jsp:include page="../pages/${PathContent}" /></div>
</main>
	<script type="text/javascript" src="/template/js/pagination.js"></script>
	<script type="text/javascript"
		src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="/materialize/js/materialize.min.js"></script>
</body>
</html>
